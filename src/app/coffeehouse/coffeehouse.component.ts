import { Component, OnInit, Renderer2, ElementRef, ViewChild } from '@angular/core';
import { GoogleLoginProvider, FacebookLoginProvider, AuthService } from 'angular-6-social-login';
import { SocialLoginModule, AuthServiceConfig } from 'angular-6-social-login';
import { Socialusers } from '../Models/socialusers'
import { Router } from '@angular/router';
import { BackendApiService } from '../services/backend-api.service';
import { Gtag } from 'angular-gtag';

@Component({
	selector: 'app-coffeehouse',
	templateUrl: './coffeehouse.component.html',
	styleUrls: ['./coffeehouse.component.css']
})
export class CoffeehouseComponent implements OnInit {
	sessionJoined: boolean = false;
	timerStarted: boolean = false
	sessionCompleted: boolean = false
	fiveMinuteTimer: boolean = false;
	maintimeInterval: any
	upcomingtime: any;
	sessionLoad = false
	totalTime = 0;
	guestOrUser: any = "";
	loggedInUserName = "";
	email: any = "";
	password: any = "";
	responseError: any = [];
	response;
	socialusers = new Socialusers();
	base_url_media: any = ""
	ShowSubmitLoader: boolean = false;
	name: any = ""
	helpingWith: any = ""
	passwordLogin: any = "";
	emailLogin: any = ""

	headline = ""
	selefDescription = ""
	imageUser: any = "assets/images/dummy-profile.png";
	varImgUrl: any = "https://focustogether.com/cherry-backend/public/uploads/";


	social_id = ""
	social_image = ""
	login_type;
	joinCoffeeSessionBool: boolean = false;
	volumeSlider: any = 50
	audioPlayer: any
	focusText: any = "";
	addTeXt: boolean = false;
	textFocus: any = "";
	timer;
	timerStart: any;
	timeSpentOnSite
	videoMode = 1
	firstImage = "assets/images/communityPeopleImage/image1.png";
	onlineUsers = [
		"assets/images/communityPeopleImage/image1.png",
		"assets/images/communityPeopleImage/image2.png",
		"assets/images/communityPeopleImage/image3.png",
		"assets/images/communityPeopleImage/image4.png",
		"assets/images/communityPeopleImage/image5.png",
		"assets/images/communityPeopleImage/image6.png",
		"assets/images/communityPeopleImage/imageM.png",
		"assets/images/communityPeopleImage/imageN.png"
	];
	musicArray: any = [];
	ambientCheck: any = [];
	showFocusRadio = 0

	timer2;
	randomNumberArray:any = [];

	selfDesc:any= "";

	time1;
	time2;
	time3;


	@ViewChild('toggleButton',null) toggleButton: ElementRef;
	@ViewChild('menu',null) menu: ElementRef;
	@ViewChild('campFireVideo',null) campFireVideo: ElementRef;

	constructor(
		public OAuth: AuthService,
		private router: Router,
		private signupService: BackendApiService,
		private renderer: Renderer2,
		private gtag: Gtag
	) {
			this.renderer.listen('window', 'click',(e:Event)=>{
				/**
				 * Only run when toggleButton is not clicked
				 * If we don't check this, all clicks (even on the toggle button) gets into this
				 * section which in the result we might never see the menu open!
				 * And the menu itself is checked here, and it's where we check just outside of
				 * the menu and button the condition abbove must close the menu
				 */
				if (
					e.target !== this.toggleButton.nativeElement &&
					e.target !== this.menu.nativeElement &&
					!this.menu.nativeElement.contains(e.target)
				)
				{
					this.showFocusRadio=0;
				}
		   });
		}

	ngOnInit() {
		this.checkGuestOrUser();
		this.timecalculator()

		if (localStorage.getItem('image') != null) {
			var str = localStorage.getItem('image');
			var n = str.includes("https://");
			if (n) {
				this.imageUser = localStorage.getItem('image')
			} else {
				// this.imageUser = this.varImgUrl + '' + localStorage.getItem('image');
				var user_name = localStorage.getItem('name');
				var nameFChar = user_name.charAt(0); 
				
				this.imageUser = "assets/images/nameschar/"+nameFChar.toLowerCase()+".png"
			}
			this.appendImage()
			this.joinCoffeeSession()
		}
		// document.getElementById("CoffeeHouseBgMusic")
		/* this.audioPlayer = <HTMLVideoElement>document.getElementById("CoffeeHouseBgMusic");
		this.audioPlayer.play(); */

		this.randomNumberArray[0] = Math.floor(Math.random() * (10000 - 100 + 1) + 100);
		this.randomNumberArray[1] = Math.floor(Math.random() * (10000 - 100 + 1) + 100);
		this.randomNumberArray[2] = Math.floor(Math.random() * (10000 - 100 + 1) + 100);

		this.timeCounter1(0)
		this.timeCounter2(1)
		this.timeCounter3(2)
	}

	logout() {


		localStorage.removeItem('name')
		localStorage.removeItem('id')
		localStorage.removeItem('image')
		localStorage.removeItem('token')
		this.imageUser = "assets/images/dummy-profile.png";
		// this.loggedInId = "";
		this.loggedInUserName = "";
		this.checkGuestOrUser()
		this.sessionJoined = false
		this.joinCoffeeSessionBool = false;

		this.onlineUsers.splice(0,1)
		this.onlineUsers.unshift(this.firstImage)
	}
	timecalculator() {
		// alert("wer")
		var current = new Date();
		var hours = current.getHours();
		var minutes = current.getMinutes();
		var seconds = current.getSeconds();
		if (minutes >= 0 && minutes < 25) {
			this.upcomingtime = hours + ":25:00";
		} else if (minutes >= 30 && minutes <= 55) {
			this.upcomingtime = hours + ":55:00";
		} else if (minutes > 55) {
			var h = hours + 1
			this.upcomingtime = h + ":25:00";
		}
		// console.log(minutes)
		if ((minutes >= 25 && minutes < 30) || (minutes >= 55)) {
			if (minutes >= 25 && minutes < 30) {
				this.upcomingtime = hours + ":30:00";
			} else if (minutes >= 55) {
				var h = hours + 1
				this.upcomingtime = h + ":00:00";
			}
			this.fiveMinuteTimer = true;
			// return;
		}

		var today = new Date();
		var dd = String(today.getDate()).padStart(2, '0');
		var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
		var yyyy = today.getFullYear();

		var todays = mm + '/' + dd + '/' + yyyy;
		var countDownDate = new Date(todays + " " + this.upcomingtime).getTime();


		var that = this;
		this.maintimeInterval = setInterval(function () {


			var now = new Date().getTime();

			var distance = countDownDate - now;

			var days = Math.floor(distance / (1000 * 60 * 60 * 24));
			var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
			var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
			var seconds = Math.floor((distance % (1000 * 60)) / 1000);

			if (minutes == 0 && seconds == 0 && that.fiveMinuteTimer) {
				let audioPlayer = <HTMLVideoElement>document.getElementById("beforesessionStartingSound");
				audioPlayer.play();

			}

			if (minutes == 0 && seconds == 1 && !that.fiveMinuteTimer) {
				let audioPlayer = <HTMLVideoElement>document.getElementById("bellSound");
				audioPlayer.play()
				// that.focusSession = that.focusSession + 1
				// localStorage.setItem("focusSession", String(that.focusSession))
				// that.showFeedback = true;
				// document.getElementById('hideorshowADiv').style.display = "none"
				// that.updateFocusSessionCounter();
			}
			if (minutes >= 0 && seconds > 0) {
				if (seconds < 10) {
					var secondsss = 0 + "" + seconds
				} else {
					var secondsss = String(seconds)
				}
				document.getElementById("timer").innerHTML = minutes + ":" + secondsss;
			}


			// If the count down is over, write some text 
			if (distance < 0) {
				clearInterval(that.maintimeInterval);
				if (that.sessionJoined && that.timerStarted) {
					that.sessionJoined = false;
					that.timerStarted = false;
					var disableClass = document.getElementsByClassName('add-main-disable-class')
					for (var i = 0; i < disableClass.length; i++) {
						disableClass[i].classList.remove('main-disable')
					}
					// document.getElementById('privateSessionId').style.pointerEvents = "";
				}
				// console.log(that.fiveMinuteTimer,that.sessionJoined)
				if (that.fiveMinuteTimer) {
					if (that.sessionJoined) {
						that.timerStarted = true;
						var disableClass = document.getElementsByClassName('add-main-disable-class')
						for (var i = 0; i < disableClass.length; i++) {
							disableClass[i].classList.add('main-disable')
						}
						// document.getElementById('privateSessionId').style.pointerEvents = "none";
						let audioPlayer = <HTMLVideoElement>document.getElementById("sessionStartingSound");
						audioPlayer.play();

					}
					that.fiveMinuteTimer = false
					that.timecalculator()
				} else {
					that.fiveMinuteTimer = true
					that.timecalculator()
					// document.getElementById('privateSessionId').style.pointerEvents = "";
				}

			}
		}, 1000, this);
	}

	checkGuestOrUser() {

		if (localStorage.getItem('id') == null) {
			this.guestOrUser = "guest";
		} else {
			this.guestOrUser = "user";
			this.loggedInUserName = localStorage.getItem('name')
			this.selfDesc =  localStorage.getItem('self_description')
			this.appendImage()
		}
		// console.log(this.guestOrUser)
		if (localStorage.getItem('timeSpent') == null) {
			this.totalTime = 0
		} else {
			this.totalTime = parseInt(localStorage.getItem('timeSpent'));
		}
		var that = this;
		var that = this;
		setInterval(() => {                           //<<<---using ()=> syntax
			that.totalTime = that.totalTime + 60;
			localStorage.setItem("timeSpent", String(that.totalTime))
		}, 60000);
	}


	public socialSignIn(socialProvider: string) {
		let socialPlatformProvider;
		if (socialProvider == 'facebook') {
			socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
		} else if (socialProvider == 'google') {
			socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
		}

		this.OAuth.signIn(socialPlatformProvider).then(socialusers => {
			console.log(socialProvider, socialusers);
			// console.log(socialusers);  
			this.Savesresponse(socialusers);

		});
	}

	submitsignupform() {

		this.ShowSubmitLoader = true;
		if (this.login_type == 2) {
			var type = 2;
		} else {
			var type = 1;
		}

		if(this.email == null){
			this.email = localStorage.getItem("social_email");
		}

		let data = {
			email: this.email,
			password: this.password,
			name: this.name,
			helping_with: this.helpingWith,
			login_type: type,
			headline: this.headline,
			selefDescription: this.selefDescription,
			social_id: this.social_id,
			image: this.social_image
		}
		this.signupService.signup(data).subscribe(
			response => {
				if (response['error'] == 1) {
					this.ShowSubmitLoader = false;
				} else {
					this.ShowSubmitLoader = false;
					// Swal.fire({
					//   title: 'Success',
					//   text: 'Signed up successfully ! Please log in.',
					//   icon: 'success',
					//   confirmButtonText: 'OK'
					// })
					this.responseError['emailErrorLogin'] = "";
					this.responseError['email_taken_message'] = "";
					localStorage.setItem("token", response['data']['token'])
					localStorage.setItem("id", response['data']['id'])
					localStorage.setItem("name", response['data']['name'])
					localStorage.setItem("image", response['data']['image'])


					var str = response['data']['image'];
					var n = str.includes("https://");
					if (n) {
						this.imageUser = response['data']['image']
					} else {
						// this.imageUser = this.varImgUrl + '' + response['data']['image'];
						var user_name = localStorage.getItem('name');
						var nameFChar = user_name.charAt(0); 
						this.imageUser = "assets/images/nameschar/"+nameFChar.toLowerCase()+".png"
					}

					// this.appendImage()

					// this.imageUser =this.varImgUrl+''+response['data']['image']
					this.checkGuestOrUser()
					document.getElementById('closeLoginModel').click();
					document.getElementById('finalCloseModal').click();
					document.getElementById('closeGetThingsModel').click();
					this.emptyVar()

					this.joinCoffeeSession()
					// document.getElementById('signinButton').click();
				}
			},
			err => {
				this.ShowSubmitLoader = false;
				console.log("err", err)
			}
		);
	}

	emptyVar() {
		this.email = "";
		this.password = "";
		this.responseError = [];

		this.ShowSubmitLoader = false;
		this.name = ""
		this.helpingWith = ""
		this.passwordLogin = "";
		this.emailLogin = ""

		this.headline = ""
		this.selefDescription = ""

		this.social_id = ""
		this.social_image = ""
		this.login_type = 3;

		this.focusText = "";
		this.addTeXt = false;
		this.textFocus = "";
	}

	signin() {
		let data = {
			email: this.emailLogin,
			password: this.passwordLogin,
		}
		this.ShowSubmitLoader = true;
		this.signupService.signin(data).subscribe(
			response => {
				this.ShowSubmitLoader = false;
				if (response['error'] == 1) {
					this.responseError['emailErrorLogin'] = true;
					this.responseError['email_taken_message'] = response['message'];
				} else {
					this.responseError['emailErrorLogin'] = "";
					this.responseError['email_taken_message'] = "";
					localStorage.setItem("token", response['data']['token'])
					localStorage.setItem("id", response['data']['id'])
					localStorage.setItem("name", response['data']['name'])
					localStorage.setItem("image", response['data']['image'])
					localStorage.setItem("breatingSession", String(response['data']['breathing_session']))
					localStorage.setItem("self_description", response['data']['headline'])
					this.emptyVar()
					// this.datCounter()

					var str = response['data']['image'];
					var n = str.includes("https://");
					if (n) {
						this.imageUser = response['data']['image']
					} else {
						// this.imageUser = this.varImgUrl + '' + response['data']['image'];
						var user_name = localStorage.getItem('name');
						var nameFChar = user_name.charAt(0); 
						this.imageUser = "assets/images/nameschar/"+nameFChar.toLowerCase()+".png"
					}

					// this.appendImage()

					this.checkGuestOrUser()
					document.getElementById('closeLoginModel').click();
					this.joinCoffeeSession()
				}

			},
			err => {
				this.ShowSubmitLoader = false;
				console.log("err", err)
			}
		);
	}



	Savesresponse(socialusers: Socialusers) {
		this.signupService.socialSignup(socialusers).subscribe(
			response => {
				if (response['data'].length == 0) {
					this.email = socialusers.email;
					localStorage.setItem("social_email", this.email)

					this.social_id = socialusers.id;
					this.social_image = socialusers.image;
					this.ShowSubmitLoader = false;
					this.login_type = 2;

					document.getElementById("openSecondModel").click();
					this.responseError['email_taken'] = false;
					this.responseError['email_taken_message'] = "";
					var close = document.getElementsByClassName('close');
					for (var i = 0; i < close.length; i++) {
						let element: HTMLElement = close[i] as HTMLElement;
						element.click();
					}
				} else {
					this.responseError['emailErrorLogin'] = "";
					this.responseError['email_taken_message'] = "";
					localStorage.setItem("token", response['data']['token'])
					localStorage.setItem("id", response['data']['id'])
					localStorage.setItem("name", response['data']['name'])
					localStorage.setItem("image", response['data']['image'])
					localStorage.setItem("self_description", response['data']['headline'])

					var str = response['data']['image'];
					var n = str.includes("https://");
					console.log(482,n)
					if (n) {
						this.imageUser = response['data']['image']
					} else {
						// this.imageUser = this.varImgUrl + '' + response['data']['image'];
						var user_name = localStorage.getItem('name');
						var nameFChar = user_name.charAt(0); 
						this.imageUser = "assets/images/nameschar/"+nameFChar.toLowerCase()+".png"
					}
					// this.appendImage()

					this.emptyVar()
					// this.imageUser =this.varImgUrl+''+response['data']['image']

					this.checkGuestOrUser()
					var close = document.getElementsByClassName('close');
					for (var i = 0; i < close.length; i++) {
						let element: HTMLElement = close[i] as HTMLElement;
						element.click();
					}
					document.getElementById('closeLoginModel').click();
					this.joinCoffeeSession()
				}

			},
			err => {
				console.log("err", err)
			}
		);
	}


	CheckEmail() {
		let data = {
			email: this.email,
			password: this.password,
		}
		this.ShowSubmitLoader = true;
		this.signupService.checkemail(data).subscribe(
			response => {
				console.log("response", response)
				if (response['error'] == 1) {
					this.ShowSubmitLoader = false;
					this.responseError['email_taken'] = true;
					this.responseError['email_taken_message'] = response['data']['message'];
				} else {
					this.ShowSubmitLoader = false;
					document.getElementById("closeSecondModal").click();
					document.getElementById("openThirdModel").click();
					this.responseError['email_taken'] = false;
					this.responseError['email_taken_message'] = "";

				}
			},
			err => {
				this.ShowSubmitLoader = false;
				console.log("err", err)
			}
		);
	}

	joinCoffeeSession() {
		this.joinCoffeeSessionBool = !this.joinCoffeeSessionBool
		let audioPlayer = <HTMLVideoElement>document.getElementById("postSound");
		audioPlayer.play();
		this.startCounting()
		var image = new Image(1, 1); 
		image.src = "//www.googleadservices.com/pagead/conversion/AW-1049912176/?label=UWAMCJLhhoECEPDG0fQD&script=0";  
	}

	leaveSession() {
		this.joinCoffeeSessionBool = !this.joinCoffeeSessionBool
		clearInterval(this.timer);
	}

	/* ChangeVolume() {
		var uncheck = document.getElementsByTagName('audio');
		for (var i = 0; i < uncheck.length; i++) {
			var ss: HTMLAudioElement = <HTMLAudioElement>uncheck[i];
			ss.volume = this.volumeSlider / 100;
		}
		this.audioPlayer.volume = this.volumeSlider / 100;
	} */

	AddFocus() {
		this.addTeXt = !this.addTeXt
	}

	postFocusText() {
		this.addTeXt = !this.addTeXt
		this.focusText = this.textFocus;
	}

	startCounting() {
		this.timerStart = 0;
		var that = this;
		this.timer = setInterval(function () {
			that.timerStart = that.timerStart + 1;
			that.timeSpentOnSite = that.secondsToHms(that.timerStart)
		}, 1000);
	}

	// getTimeSpentOnSite(){
	//   this.timeSpentOnSite = parseInt(localStorage.getItem('timeSpentOnSite'));
	//   this.timeSpentOnSite = isNaN(this.timeSpentOnSite) ? 0 : this.timeSpentOnSite;
	//   return  this.timeSpentOnSite;
	// }

	secondsToHms(d) {
		d = Number(d);
		var h = Math.floor(d / 3600);
		var m = Math.floor(d % 3600 / 60);
		var s = Math.floor(d % 3600 % 60);

		var hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : "";
		var mDisplay = m > 0 ? m + (m == 1 ? " minute, " : " minutes, ") : "";
		var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
		if (h > 0) {
			return hDisplay
		}
		if (m > 0) {
			return mDisplay
		} else {
			return sDisplay
		}
		return hDisplay + mDisplay + sDisplay;
	}

	switchVideoMode(vm,onPopUp){
		this.videoMode = vm
		if(onPopUp == 'yes'){
			document.getElementById('closeVideoMode').click();
			document.getElementById('openGetThingsModel').click();
		}	
	}

	learnMore(){
		document.getElementById('openLearnMoreModel').click();
	}

	winAwards(){
		document.getElementById('closeLearnMoreModel').click();
		document.getElementById('openWeeklyAwardsModel').click();
	}

	communityHelpOnline(){
		document.getElementById('openCommHelpModel').click();
	}	

	appendImage(){
		this.onlineUsers.splice(0,1)
		this.onlineUsers.unshift(this.imageUser)
	}

	music(type, event) {

		this.musicArray = [];
		var uncheck = document.getElementsByClassName('musicCheckbox');
		for (var i = 0; i < uncheck.length; i++) {
			var ss: HTMLInputElement = <HTMLInputElement>uncheck[i];
			if (ss.className != event.target.className) {
				ss.checked = false;
			}
		}
		// event.target.checked = true;
		if (event.target.checked) {
			localStorage.setItem("musicCheckArray", type);
			var o = {};
			o[type] = true;
			this.musicArray.push(o)
			setTimeout(() => {
				let audioPlayer = <HTMLVideoElement>document.getElementById(type + "Sound");
				audioPlayer.play();
				// audioPlayer.volume = this.volumeSlider/100;
			}, 100);
	
		} else {
			localStorage.removeItem("musicCheckArray");
			let audioPlayer = <HTMLVideoElement>document.getElementById(type + "Sound");
			audioPlayer.pause();
			audioPlayer.currentTime = 0;
			this.musicArray = [];
		}
	
	}

	checkMusic(music) {
		// console.log(this.ambientCheck);
		if (this.ambientCheck.includes(music)) {
			let audioPlayer = <HTMLVideoElement>document.getElementById(music + "Sound");
			audioPlayer.play();
			// audioPlayer.volume = parseFloat(this.volumeSlider)/100;
			// this.pauseOrPlay()
			return true;
		}
	}

	ambientMusic(type, event) {
		// console.log(this.ambientCheck)
		// console.log(event.target.checked)
		if (event.target.checked) {
			this.ambientCheck.push(type)
		} else {
			var index = this.ambientCheck.indexOf(type);
			if (index !== -1) {
				this.ambientCheck.splice(index, 1);
			}
		}
		let audioPlayer = <HTMLVideoElement>document.getElementById(type + "Sound");
		if (event.target.checked) {
			audioPlayer.play();
			// audioPlayer.volume = parseFloat(this.volumeSlider)/100;
		} else {
			audioPlayer.pause();
			audioPlayer.currentTime = 0;
		}
		// / console.log(this.ambientCheck)
		localStorage.setItem("ambientCheck", this.ambientCheck);

	}

	focusRadio(){
		this.showFocusRadio = (this.showFocusRadio == 0)?1:0;
	}

	/* playVideo(video){

		// this.campFireVideo.startVideo();
	} */

	id = 'qDuKsiwS5xw';

	playerVars = {
		cc_lang_pref: 'en'
	};
	
	public player;
	public ytEvent;

	onStateChange(event) {
		this.ytEvent = event.data;
	}
	savePlayer(player) {
		this.player = player;
		this.volChange()
	}
	
	playVideo() {
		this.player.playVideo();
	}
	
	pauseVideo() {
		this.player.pauseVideo();
	}
	volChange() {
		this.player.setVolume(this.volumeSlider);
	}

	changeAudio(videoId){
		console.log(this.ambientCheck)
		if(this.id == videoId ){
			this.player.pauseVideo();
			this.id = "";
		}
		else{
			this.id = videoId
			this.player.loadVideoById(videoId);
		}
	}

	ngOnDestroy() {
		clearInterval(this.timer);
		clearInterval(this.timer2);
	}

	timeCounter1( index){
		var that = this;
		var int1=setInterval(function () {
			that.randomNumberArray[index] = that.randomNumberArray[index] + 1;
			that.time1 = that.secondsToHms(that.randomNumberArray[index])
		}, 1000);
	}

	timeCounter2(index){
		var that = this;
		var int2=setInterval(function () {
			that.randomNumberArray[index] = that.randomNumberArray[index] + 1;
			that.time2 = that.secondsToHms(that.randomNumberArray[index])
		}, 1000);
	}

	timeCounter3(index){
		var that = this;
		var int3= setInterval(function () {
			that.randomNumberArray[index] = that.randomNumberArray[index] + 1;
			that.time3 = that.secondsToHms(that.randomNumberArray[index])
		}, 1000);
	}
}



