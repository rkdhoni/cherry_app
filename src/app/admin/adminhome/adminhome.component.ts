import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { BackendApiService } from 'src/app/services/backend-api.service';
@Component({
  selector: 'app-adminhome',
  templateUrl: './adminhome.component.html',
  styleUrls: ['./adminhome.component.css']
})
export class AdminhomeComponent implements OnInit {
usersData:any=[];
varImgUrl: any = "https://focustogether.com/cherry-backend/public/uploads/";
page:any = 1;
  constructor(private router: Router, private signupService: BackendApiService) { }

  ngOnInit() {
    // alert()
    if (localStorage.getItem('role') != "admin") {
      this.router.navigate(['admin'])
    }
    else{
      this.getUsers()
    }
  }

  logout() {
    localStorage.removeItem("role");
    localStorage.removeItem("tokenAdmin");
    this.router.navigate(['admin/']);
  }


  getUsers(){
    var data={
      page:this.page
    }
    this.signupService.getAllUsers(data).subscribe(
      response => {
        for(var i = 0 ; i <response['data']['data'].length ; i++){
          this.usersData.push(response['data']['data'][i])  
        }
        // this.usersData = response['data']['data'];
        console.log(this.usersData)
      },
      err => {
        console.log("err",err)
      }
    );
  }

  loadmore(){
    // alert("asd")
    this.page = this.page + 1
    this.getUsers()
  }

}
