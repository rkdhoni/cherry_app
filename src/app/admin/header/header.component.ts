import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BackendApiService } from 'src/app/services/backend-api.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router, private signupService: BackendApiService) { }

  ngOnInit() {
  }

  logout() {
    localStorage.removeItem("role");
    localStorage.removeItem("tokenAdmin");
    this.router.navigate(['admin/']);
  }

}
