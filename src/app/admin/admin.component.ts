import { Component, OnInit } from '@angular/core';

import { GoogleLoginProvider, FacebookLoginProvider, AuthService } from 'angular-6-social-login';  
import { SocialLoginModule, AuthServiceConfig } from 'angular-6-social-login';  
import { Socialusers } from '../Models/socialusers'  
// import { SocialloginService } from '../Service/sociallogin.service';  
import { Router, ActivatedRoute, Params } from '@angular/router';
import { BackendApiService } from '../services/backend-api.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  email:any="";
  password:any="";
  errorMsg:any="";
  ShowSubmitLoader:boolean = false;
  constructor(public OAuth: AuthService,   
    private router: Router ,private signupService:BackendApiService) { }

  ngOnInit() {
    // this.getUsers()
  }

  // getUsers(){
  //   this.signupService.getAllUsers().subscribe(
  //     response => {
        
  //     },
  //     err => {
  //       console.log("err",err)
  //     }
  //   );
  // }

  submitAdminForm(){
    this.ShowSubmitLoader =true;
    var data = {
      email:this.email,
      password:this.password
    }
    this.signupService.siginAdmin(data).subscribe(
      response => {
        if(response['error'] == 1){
          this.errorMsg = response['message']
          this.ShowSubmitLoader =false;
        }else{
          localStorage.setItem("role","admin")
          localStorage.setItem("tokenAdmin",response['data']['token'])
          this.router.navigate(['admin/home']);
        }
        
      },
      err => {
        console.log("err",err)
      }
    );
  }

}
