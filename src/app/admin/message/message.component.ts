import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BackendApiService } from 'src/app/services/backend-api.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {
  msgData:any = [];
  dayCount:any=""
  messageText:any=""
  varImgUrl: any = "https://focustogether.com/cherry-backend/public/uploads/";
  private formData = new FormData();

  imageSrc: string | ArrayBuffer;
  constructor(private router: Router, private signupService: BackendApiService) { }

  ngOnInit() {
    if (localStorage.getItem('role') != "admin") {
      this.router.navigate(['admin'])
    }
    else{
      this.getMessages()
    }
  }

  getMessages(){
    this.signupService.getMessages().subscribe(
      response => {
        this.msgData = response['message']
        console.log(this.msgData)
      },
      err => {
        console.log("err",err)
      }
    );
  }
  readURL(event) 
  { 
    if (event.target.files && event.target.files[0]) 
      { const file = event.target.files[0]; 
        const reader = new FileReader(); 
        reader.onload = e => this.imageSrc = reader.result; 
        // this.files = event.target.files;
        //console.log.log(event.target.files)
        this.formData.append("file", event.target.files[0]);
        // console.log(this.formData)
        reader.readAsDataURL(file); 
      } 
  }
  addMessage(){
    this.formData.append("message", this.messageText);
    this.formData.append("dayCount", this.dayCount);
    this.signupService.addMessage(this.formData).subscribe(
      response => {
        this.getMessages()
      },
      err => {
        console.log("err",err)
      }
    );
  }

}
