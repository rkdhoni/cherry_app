import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BackendApiService } from 'src/app/services/backend-api.service';

@Component({
  selector: 'app-video-view',
  templateUrl: './video-view.component.html',
  styleUrls: ['./video-view.component.css']
})
export class VideoViewComponent implements OnInit {
  videosData:any = [];
  varImgUrl: any = "https://focustogether.com/cherry-backend/public/uploads/";
  video_name:any=""
  private formData = new FormData();
  private formDataEdit = new FormData();
  imageSrc: string | ArrayBuffer;
  video_name_edit:any="";
  activeId = "";
  constructor(private router: Router, private signupService: BackendApiService) { }

  ngOnInit() {
    if (localStorage.getItem('role') != "admin") {
      this.router.navigate(['admin'])
    }
    else{
      this.getvideos()
    }
  }

  getvideos(){
    this.signupService.getvideos().subscribe(
      response => {
        console.log(response)
        this.videosData = response['video']
        console.log(this.videosData)
      },
      err => {
        console.log("err",err)
      }
    );
  }

  deleteVideos(id){
    // alert(id)
    var video = {
      id:id
    }
    this.signupService.deleteVideos(video).subscribe(
      response => {
        this.getvideos()
      },
      err => {
        console.log("err",err)
      }
    );
  }

  readURL(event) 
  { 
    if (event.target.files && event.target.files[0]) 
      { const file = event.target.files[0]; 
        const reader = new FileReader(); 
        reader.onload = e => this.imageSrc = reader.result; 
        // this.files = event.target.files;
        //console.log.log(event.target.files)
        this.formData.append("file", event.target.files[0]);
        // console.log(this.formData)
        reader.readAsDataURL(file); 
      } 
  }

  readURL2(event) 
  { 
    if (event.target.files && event.target.files[0]) 
      { const file = event.target.files[0]; 
        const reader = new FileReader(); 
        reader.onload = e => this.imageSrc = reader.result; 
        // this.files = event.target.files;
        //console.log.log(event.target.files)
        this.formDataEdit.append("file", event.target.files[0]);
        // console.log(this.formData)
        reader.readAsDataURL(file); 
      } 
  }

  uploadVideo(){
    this.formData.append("name", this.video_name);
    this.signupService.uploadVideo(this.formData).subscribe(
      response => {
        this.getvideos()
      },
      err => {
        console.log("err",err)
      }
    );
  }

  editVideos(post){
    // console.log(post)
    this.video_name_edit = post.video_name;
    this.activeId = post.id;
  }

  uploadVideoEdit(){
    this.formDataEdit.append("name", this.video_name_edit);
    this.formDataEdit.append("id", this.activeId);
    this.signupService.uploadVideoedit(this.formDataEdit).subscribe(
      response => {
        this.getvideos()
      },
      err => {
        console.log("err",err)
      }
    );
  }



}
