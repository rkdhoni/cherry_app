import { Component, OnInit } from '@angular/core';
import { BackendApiService } from '../services/backend-api.service';
import Swal from 'sweetalert2'
import { Router } from '@angular/router';
@Component({
  selector: 'app-update-profile',
  templateUrl: './update-profile.component.html',
  styleUrls: ['./update-profile.component.css']
})
export class UpdateProfileComponent implements OnInit {
name:string="";
role:string="";
tagline:string="";
dateJoined:any=""
image:any=""
imageSrc: string | ArrayBuffer;
files: any[];
ShowSubmitLoader:boolean = false;
imageUser:any="assets/images/dummy-profile.png";
varImgUrl:any = "https://focustogether.com/cherry-backend/public/uploads/";
loggedInUserName = ""
private formData = new FormData();
  constructor(private updateService: BackendApiService,private router: Router) { }

  ngOnInit() {
    console.log(localStorage.getItem('token'))
    this.getData();
    if(localStorage.getItem('image')!=null ){
      var str = localStorage.getItem('image');
      var n = str.includes("https://");
      if(n){
        this.imageUser =localStorage.getItem('image')
      }else{
        this.imageUser = this.varImgUrl+''+localStorage.getItem('image');
      }
      // this.imageUser = this.varImgUrl+''+localStorage.getItem('image');
      console.log("this.imageUser",this.imageUser)
    }
    this.loggedInUserName = localStorage.getItem('name')
  }

  getData(){
    this.updateService.getUserData().subscribe(
      response => {
        console.log(response)
        this.name = response['data'].name
        this.role = response['data'].self_description
        this.tagline = response['data'].headline
        var d = new Date(response['data'].created_at);
        this.image= response['data'].image
        var str = this.image;
        var n = str.includes("https://");
        if(n){
          this.image =this.image
        }else{
          this.image = this.varImgUrl+''+this.image;
        }
        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May","June","July", "Aug", "Sept", "Oct", "Nov","Dec"];
        console.log(monthNames[d.getMonth()]);
        console.log(d.getFullYear());

        this.dateJoined = "Joined "+monthNames[d.getMonth()]+" "+ d.getFullYear()
      },
      err => {
        console.log(err)
      }
    );
  }

  editProfile(){
    document.getElementById('fileUpload').click();
  }

  readURL(event) 
  { 
    if (event.target.files && event.target.files[0]) 
      { const file = event.target.files[0]; 
        const reader = new FileReader(); 
        reader.onload = e => this.imageSrc = reader.result; 
        this.files = event.target.files;
        //console.log.log(event.target.files)
        this.formData.append("file", event.target.files[0]);
        console.log(this.formData)
        reader.readAsDataURL(file); 
      } 
  }


  save(){
    this.formData.append("name", this.name);
    this.formData.append("role", this.role);
    this.formData.append("tagline", this.tagline);
    this.ShowSubmitLoader = true;
    this.updateService.updateUserData(this.formData).subscribe(
      response => {
        localStorage.setItem("name",response['data']['name'])
        localStorage.setItem("image",response['data']['image'])
        this.imageUser = this.varImgUrl+''+localStorage.getItem('image');
        var str = localStorage.getItem('image');
        var n = str.includes("https://");
        if(n){
          this.imageUser =localStorage.getItem('image')
        }else{
          this.imageUser = this.varImgUrl+''+localStorage.getItem('image');
        }
        this.loggedInUserName = response['data']['name']
        console.log(response)
        this.ShowSubmitLoader = false;
        Swal.fire({
          title: 'Success',
          text: 'Your profile has been updated successfully',
          icon: 'success',
          confirmButtonText: 'OK'
        })
        
      },
      err => {
        Swal.fire({
          title: 'Error!',
          text: 'Something went wrong!',
          icon: 'error',
          confirmButtonText: 'OK'
        })
      }
    );
  }

  logout(){
    localStorage.removeItem('name')
    localStorage.removeItem('id')
    localStorage.removeItem('image')
    localStorage.removeItem('token')
    this.imageUser = "assets/images/dummy-profile.png";
    this.router.navigate(['/']);
  }
}
