import { Component, OnInit } from '@angular/core';
import { GoogleLoginProvider, FacebookLoginProvider, AuthService } from 'angular-6-social-login';
import { SocialLoginModule, AuthServiceConfig } from 'angular-6-social-login';
import { Socialusers } from '../Models/socialusers'
// import { SocialloginService } from '../Service/sociallogin.service';  
import { Router, ActivatedRoute, Params } from '@angular/router';
import { BackendApiService } from '../services/backend-api.service';


// import 'rxjs/add/operator/map';
import * as firebase from 'firebase';
import { AngularFirestoreCollection, AngularFirestore, AngularFirestoreDocument } from "angularfire2/firestore";
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2'

// import undefined = require('firebase/empty-import');

// CommonJS
// const Swal = require('sweetalert2')
interface Post {
	title: string;
	content: string;
}
@Component({
	selector: 'app-mindful-session',
	templateUrl: './mindful-session.component.html',
	styleUrls: ['./mindful-session.component.css']
})
export class MindfulSessionComponent implements OnInit {
	timeoutHandle: any;
	sessionJoined: boolean = false;
	timerStarted: boolean = false
	sessionCompleted: boolean = false
	musicArray: any = [];
	chimesArray: any = [];
	ambientCheck: any = [];
	guestOrUser: any = "";
	totalTime = 0
	focusSession = 0;
	dayCounterJson: any;
	dayCounter: any;
	breatingSeassion = 0;

	focusText = "";

	postsCol: AngularFirestoreCollection<Post>;
	posts: Observable<Post[]>;

	privateTimer: boolean = false
	privateTimeCount: any = []
	stopTimer: boolean = false;
	message: any = []

	chatbox: any = ""
	chatReference: any;
	messageReference: any;

	liveUserArray: any = [];
	liveUserArrayCurrentFocus: any = [];
	liveUserShowUI: any = [];
	pagintionLiveUser: boolean = false
	//signup signin
	email: any = "";
	password: any = "";
	responseError: any = [];
	socialusers = new Socialusers();
	ShowSubmitLoader: boolean = false;
	name: any = ""
	helpingWith: any = ""
	passwordLogin: any = "";
	emailLogin: any = ""
	loggedInUserName = ""
	headline = ""
	selefDescription = ""


	profileData: any = []

	showFeedback: boolean = false
	rating: any = "";
	imageUser: any = "assets/images/dummy-profile.png";
	varImgUrl: any = "https://focustogether.com/cherry-backend/public/uploads/";
	loggedInId: any = "";
	upcomingtime: any;
	sessionLoad = false

	fiveMinuteTimer: boolean = false;
	maintimeInterval: any
	// loggedInUserName:string="Guest";
	social_id = ""
	social_image = ""
	login_type;
	constructor(private signupService: BackendApiService, public OAuth: AuthService, private afs: AngularFirestore, private db: AngularFireDatabase) { }

	ngOnInit() {

		document.getElementsByTagName("body")[0].classList.add("second-page");
		this.datCounter();
		this.getSelectedMusic()
		this.getOtherSelectedAudio()
		this.checkGuestOrUser()



		setTimeout((that) => {
			that.timecalculator()
			that.getFocusCounterAndJoinedDetail()
		}, 100, this);
		var disableClass = document.getElementsByClassName('add-main-disable-class')
		for (var i = 0; i < disableClass.length; i++) {
			disableClass[i].classList.remove('main-disable')
		}
		this.chatReference = this.afs.collection('posts');

		this.onlineOffline();
		this.getLiveuser()
		this.getAllChat()
		if (localStorage.getItem('image') != null) {
			var str = localStorage.getItem('image');
			var n = str.includes("https://");
			if (n) {
				this.imageUser = localStorage.getItem('image')
			} else {
				this.imageUser = this.varImgUrl + '' + localStorage.getItem('image');
			}
			// this.imageUser = this.varImgUrl+''+localStorage.getItem('image');
		}
	}
	ngOnDestroy() {
		var uid = localStorage.getItem('id')
		var type = "user"
		var userStatusDatabaseRef = this.db.database.ref('/status/' + uid);

		var isOfflineForDatabase = {
			state: 'offline',
			// last_changed: this.db.ServerValue.TIMESTAMP,
			id: uid,
			type: type
		};
		userStatusDatabaseRef.set(isOfflineForDatabase);
		clearInterval(this.maintimeInterval);
	}


	// countdown(minutes, seconds) {
	//   this.tick(minutes, seconds,"simle");
	// }
	checkMusic(music) {
		// console.log(this.ambientCheck);
		if (this.ambientCheck.includes(music)) {
			let audioPlayer = <HTMLVideoElement>document.getElementById(music + "Sound");
			audioPlayer.play();
			// audioPlayer.volume = parseFloat(this.volumeSlider)/100;
			// this.pauseOrPlay()
			return true;
		}
	}
	ambientMusic(type, event) {
		// console.log(this.ambientCheck)
		// console.log(event.target.checked)
		if (event.target.checked) {
			this.ambientCheck.push(type)
		} else {
			var index = this.ambientCheck.indexOf(type);
			if (index !== -1) {
				this.ambientCheck.splice(index, 1);
			}
		}
		let audioPlayer = <HTMLVideoElement>document.getElementById(type + "Sound");
		if (event.target.checked) {
			audioPlayer.play();
			// audioPlayer.volume = parseFloat(this.volumeSlider)/100;
		} else {
			audioPlayer.pause();
			audioPlayer.currentTime = 0;
		}
		// / console.log(this.ambientCheck)
		localStorage.setItem("ambientCheck", this.ambientCheck);

	}

	checkGuestOrUser() {

		if (localStorage.getItem('id') == null) {
			this.guestOrUser = "guest";
		} else {
			this.guestOrUser = "user";
			this.loggedInUserName = localStorage.getItem('name')
			this.loggedInId = localStorage.getItem('id')

		}
		if (localStorage.getItem('timeSpent') == null) {
			this.totalTime = 0
		} else {
			this.totalTime = parseInt(localStorage.getItem('timeSpent'));
		}
		if (localStorage.getItem('focusSession') == null) {
			this.focusSession = 0
		} else {
			this.focusSession = parseInt(localStorage.getItem('focusSession'));
		}
		if (localStorage.getItem('breatingSession') == null) {
			this.breatingSeassion = 0
		} else {
			this.breatingSeassion = parseInt(localStorage.getItem('breatingSession'));
		}



		var that = this;

		var that = this;
		setInterval(() => {                           //<<<---using ()=> syntax
			that.totalTime = that.totalTime + 60;
			localStorage.setItem("timeSpent", String(that.totalTime))
		}, 60000);
	}

	datCounter() {
		// alert("Sdf")
		let today = new Date().toISOString().slice(0, 10)
		var todayDate = {
			today: today
		}
		this.signupService.dateCounter(todayDate).subscribe(
			response => {
				this.dayCounter = response['message']['day_counter'];
				console.log("response['message']['last_date']", response['message']['last_date'])
				if (response['message']['last_date'] != today) {
					console.log("232")
					var data = {
						dayCounter: this.dayCounter,
						today: today
					}
					this.signupService.updatedateCounter(data).subscribe(
						response => {
							this.dayCounter = this.dayCounter + 1
						},
						err => {
							// this.ShowSubmitLoader = false;
						}
					);
				}
			},
			err => {
				// this.ShowSubmitLoader = false;
			}
		);
	}

	getFocusCounterAndJoinedDetail() {
		var todayDate = new Date().toISOString().slice(0, 10);
		var data = {
			date: todayDate,
			time: this.upcomingtime
		}
		this.signupService.getSessionData(data).subscribe(
			response => {
				if (response['data'].length > 0) {
					var d = new Date();
					var nowTime = d.getTime();
					var joined = new Date(response['data'][0].focus_end_time);
					var jj = joined.getTime()
					if (jj > nowTime && response['data'][0].session_status != 0) {
						if (!this.fiveMinuteTimer) {
							this.sessionJoined = true;
							this.timerStarted = true;
							var disableClass = document.getElementsByClassName('add-main-disable-class')
							for (var i = 0; i < disableClass.length; i++) {
								disableClass[i].classList.add('main-disable')
							}
							this.focusText = response['data'][0].post
							document.getElementById('privateSessionId').style.pointerEvents = "none";
						}
						else {
							this.sessionJoined = true;
						}
					}
				}
			},
			err => {
				// this.ShowSubmitLoader = false;
			}
		);
	}

	getDateTime() {
		var now = new Date();
		var year = now.getFullYear();
		var month = String(now.getMonth() + 1);
		var day = String(now.getDate());
		var hour = String(now.getHours());
		var minute = String(now.getMinutes());
		var second = String(now.getSeconds());
		if (month.toString().length == 1) {
			month = '0' + String(month);
		}
		if (day.toString().length == 1) {
			day = '0' + day;
		}
		if (hour.toString().length == 1) {
			hour = '0' + hour;
		}
		if (minute.toString().length == 1) {
			minute = '0' + minute;
		}
		if (second.toString().length == 1) {
			second = '0' + second;
		}
		var dateTime = year + '/' + month + '/' + day + ' ' + hour + ':' + minute + ':' + second;
		return dateTime;
	}


	music(type, event) {

		this.musicArray = [];
		var uncheck = document.getElementsByClassName('musicCheckbox');
		for (var i = 0; i < uncheck.length; i++) {
			var ss: HTMLInputElement = <HTMLInputElement>uncheck[i];
			// console.log(ss.className);
			// console.log(event.target.className);
			if (ss.className != event.target.className) {
				ss.checked = false;
			}

		}
		// event.target.checked = true;
		if (event.target.checked) {
			localStorage.setItem("musicCheckArray", type);
			var o = {};
			o[type] = true;
			this.musicArray.push(o)
			// console.log(this.musicArray,type)
			setTimeout(() => {
				let audioPlayer = <HTMLVideoElement>document.getElementById(type + "Sound");
				audioPlayer.play();
				// audioPlayer.volume = this.volumeSlider/100;
			}, 100);

		} else {
			localStorage.removeItem("musicCheckArray");
			let audioPlayer = <HTMLVideoElement>document.getElementById(type + "Sound");
			audioPlayer.pause();
			audioPlayer.currentTime = 0;
			this.musicArray = [];
		}

	}

	getOtherSelectedAudio() {
		var otherSelectedMusic = localStorage.getItem("musicCheckArray");
		setTimeout(function () {
			var classs = document.getElementsByClassName(otherSelectedMusic + 'Check');
			var ss: HTMLInputElement = <HTMLInputElement>classs[0];
			if (ss != undefined) {
				ss.click()
			}

		}, 2150);

	}
	getSelectedMusic() {
		var ambientSelectedMusic = localStorage.getItem("ambientCheck");
		if (ambientSelectedMusic != null) {
			this.ambientCheck = ambientSelectedMusic.split(',')
		}
	}

	tick(minutes, seconds, type) {
		// // alert(3)
		// var counter = document.getElementById("timer");
		// counter.innerHTML =
		//   minutes.toString() + ":" + (seconds < 10 ? "0" : "") + String(seconds);
		// seconds--;
		// if (seconds >= 0) {
		//   var that = this;
		//   this.timeoutHandle = setTimeout(function () {
		//     that.countdown(minutes, seconds);
		//   }, 1000);
		// } else {
		//   if (minutes >= 1) {
		//     var that = this;
		//     setTimeout(function () {
		//       that.countdown(minutes - 1, 59);
		//     }, 1000);
		//   }
		// }
		// if (minutes == 0 && seconds == 1 && this.sessionJoined && this.timerStarted) {
		//   let audioPlayer = <HTMLVideoElement>document.getElementById("bellSound");
		//   audioPlayer.play()
		// }
		// console.log("this.fiveMinuteTimer",this.fiveMinuteTimer)
		// if(minutes == 0 && seconds == 0){


		//   if(this.fiveMinuteTimer && (this.sessionJoined && !this.timerStarted)){
		//     this.timerStarted = true;
		//     this.fiveMinuteTimer = false;
		//     this.countdown(25, 0);
		//     document.getElementById('privateSessionId').style.pointerEvents = "";
		//     var disableClass = document.getElementsByClassName('add-main-disable-class')
		//     for (var i = 0; i < disableClass.length; i++) {
		//       disableClass[i].classList.remove('main-disable')
		//     }
		//   }
		//   if(!this.fiveMinuteTimer){
		//     this.countdown(5, 0);
		//     this.fiveMinuteTimer = true;
		//     this.timerStarted = false;
		//     this.sessionJoined = false;
		//   }else if(this.fiveMinuteTimer && !this.sessionJoined){
		//     this.countdown(25, 0);
		//   }else if(!this.fiveMinuteTimer && this.timerStarted){
		//     this.countdown(5, 0);
		//     this.fiveMinuteTimer = true;
		//     this.timerStarted = false;
		//     this.sessionJoined = false;
		//   }else if(this.fiveMinuteTimer || (this.sessionJoined && this.timerStarted)){
		//     this.fiveMinuteTimer = false;
		//     this.countdown(25, 0);
		//     this.timerStarted = false;
		//     this.sessionJoined = false;
		//     var disableClass = document.getElementsByClassName('add-main-disable-class')
		//     for (var i = 0; i < disableClass.length; i++) {
		//       disableClass[i].classList.remove('main-disable')
		//     }
		//   }


		// if (this.sessionJoined && this.timerStarted){
		//     this.countdown(5, 0);
		//     this.fiveMinuteTimer = true;
		//     document.getElementById('privateSessionId').style.pointerEvents = "";
		//     this.timerStarted = false;
		//     var disableClass = document.getElementsByClassName('add-main-disable-class')
		//     for (var i = 0; i < disableClass.length; i++) {
		//       disableClass[i].classList.remove('main-disable')
		//     }
		// }else if(!this.timerStarted && !this.fiveMinuteTimer){
		//   this.countdown(5, 0);
		//   this.fiveMinuteTimer = true;
		//   document.getElementById('privateSessionId').style.pointerEvents = "";
		//   this.timerStarted = false;
		//   var disableClass = document.getElementsByClassName('add-main-disable-class')
		//   for (var i = 0; i < disableClass.length; i++) {
		//     disableClass[i].classList.remove('main-disable')
		//   }
		// }else if(this.fiveMinuteTimer){
		//   this.countdown(25, 0);
		//   this.fiveMinuteTimer = false;
		//   document.getElementById('privateSessionId').style.pointerEvents = "";
		//   this.timerStarted = false;
		//   var disableClass = document.getElementsByClassName('add-main-disable-class')
		//   for (var i = 0; i < disableClass.length; i++) {
		//     disableClass[i].classList.remove('main-disable')
		//   }
		// }

		// }
		// if (minutes == 0 && seconds == 0) {
		//   if (this.sessionJoined && !this.timerStarted) {
		//     this.countdown(25, 0);
		//     document.getElementById('privateSessionId').style.pointerEvents = "none";
		//     this.timerStarted = true;
		//     var disableClass = document.getElementsByClassName('add-main-disable-class')
		//     for (var i = 0; i < disableClass.length; i++) {
		//       disableClass[i].classList.add('main-disable')
		//     }
		//   } else if (this.sessionJoined && this.timerStarted) {
		//     var that = this;
		//     setTimeout(function () {
		//       that.timecalculator();
		//       that.showFeedback = true;
		//       document.getElementById('hideorshowADiv').style.display = "none"
		//       that.sessionCompleted = true;
		//       that.sessionJoined = false
		//       that.timerStarted = false;
		//       that.focusSession = that.focusSession + 1
		//       that.focusText = "";
		//       localStorage.setItem("focusSession", String(that.focusSession))
		//       document.getElementById('privateSessionId').style.pointerEvents = "auto";
		//       var disableClass = document.getElementsByClassName('add-main-disable-class')
		//       for (var i = 0; i < disableClass.length; i++) {
		//         disableClass[i].classList.remove('main-disable')
		//       }
		//     }, 1000);

		//   }
		//   else {
		//     var that = this;
		//     setTimeout(function () {
		//       that.timecalculator();
		//     }, 2100);
		//   }
		// }

	}


	timecalculator() {
		// alert("wer")
		var current = new Date();
		var hours = current.getHours();
		var minutes = current.getMinutes();
		var seconds = current.getSeconds();
		if (minutes >= 0 && minutes < 25) {
			this.upcomingtime = hours + ":25:00";
		} else if (minutes >= 30 && minutes <= 55) {
			this.upcomingtime = hours + ":55:00";
		} else if (minutes > 55) {
			var h = hours + 1
			this.upcomingtime = h + ":25:00";
		}
		// console.log(minutes)
		if ((minutes >= 25 && minutes < 30) || (minutes >= 55)) {
			if (minutes >= 25 && minutes < 30) {
				this.upcomingtime = hours + ":30:00";
			} else if (minutes >= 55) {
				var h = hours + 1
				this.upcomingtime = h + ":00:00";
			}
			this.fiveMinuteTimer = true;
			// return;
		}

		var today = new Date();
		var dd = String(today.getDate()).padStart(2, '0');
		var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
		var yyyy = today.getFullYear();

		var todays = mm + '/' + dd + '/' + yyyy;
		var countDownDate = new Date(todays + " " + this.upcomingtime).getTime();


		var that = this;
		this.maintimeInterval = setInterval(function () {


			var now = new Date().getTime();

			var distance = countDownDate - now;

			var days = Math.floor(distance / (1000 * 60 * 60 * 24));
			var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
			var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
			var seconds = Math.floor((distance % (1000 * 60)) / 1000);

			if (minutes == 0 && seconds == 30 && that.fiveMinuteTimer && that.sessionJoined) {
				let audioPlayer = <HTMLVideoElement>document.getElementById("beforesessionStartingSound");
				audioPlayer.play();

			}

			if (minutes == 0 && seconds == 1 && that.sessionJoined && that.timerStarted) {
				let audioPlayer = <HTMLVideoElement>document.getElementById("bellSound");
				audioPlayer.play()
				that.focusSession = that.focusSession + 1
				localStorage.setItem("focusSession", String(that.focusSession))
				that.showFeedback = true;
				document.getElementById('hideorshowADiv').style.display = "none"
				that.updateFocusSessionCounter();
			}
			if (minutes >= 0 && seconds > 0) {
				if (seconds < 10) {
					var secondsss = 0 + "" + seconds
				} else {
					var secondsss = String(seconds)
				}
				document.getElementById("timer").innerHTML = minutes + ":" + secondsss;
			}


			// If the count down is over, write some text 
			if (distance < 0) {
				clearInterval(that.maintimeInterval);
				if (that.sessionJoined && that.timerStarted) {
					that.sessionJoined = false;
					that.timerStarted = false;
					var disableClass = document.getElementsByClassName('add-main-disable-class')
					for (var i = 0; i < disableClass.length; i++) {
						disableClass[i].classList.remove('main-disable')
					}
					document.getElementById('privateSessionId').style.pointerEvents = "";
				}
				// console.log(that.fiveMinuteTimer,that.sessionJoined)
				if (that.fiveMinuteTimer) {
					if (that.sessionJoined) {
						that.timerStarted = true;
						var disableClass = document.getElementsByClassName('add-main-disable-class')
						for (var i = 0; i < disableClass.length; i++) {
							disableClass[i].classList.add('main-disable')
						}
						document.getElementById('privateSessionId').style.pointerEvents = "none";
						let audioPlayer = <HTMLVideoElement>document.getElementById("sessionStartingSound");
						audioPlayer.play();

					}
					that.fiveMinuteTimer = false
					that.timecalculator()
				} else {
					that.fiveMinuteTimer = true
					that.timecalculator()
					document.getElementById('privateSessionId').style.pointerEvents = "";
				}

			}
		}, 1000, this);
	}

	leaveSession() {
		this.sessionJoined = false;
		this.timerStarted = false;
		var disableClass = document.getElementsByClassName('add-main-disable-class')
		for (var i = 0; i < disableClass.length; i++) {
			disableClass[i].classList.remove('main-disable')
		}
		if (this.guestOrUser != "guest") {
			this.signupService.leaveSession().subscribe(
				response => {
					console.log("done")
				},
				err => {

				}
			);

		}
		document.getElementById('privateSessionId').style.pointerEvents = "";
	}

	updateFocusSessionCounter() {
		if (this.guestOrUser != "guest") {
			this.signupService.updateFocusSessionCounter().subscribe(
				response => {
					console.log("done")
				},
				err => {
					this.ShowSubmitLoader = false;
				}
			);
		}
	}



	joinSession() {
		// console.log(this.upcomingtime)
		if (this.guestOrUser != "guest") {
			// document.getElementById('signhupforsession').click()
			var todayDate = new Date().toISOString().slice(0, 10);
			var data = {
				post: this.focusText,
				date: todayDate,
				time: this.upcomingtime,
				fiveMinuteTimer: this.fiveMinuteTimer
			}
			if (this.fiveMinuteTimer) {
				data.time = this.upcomingtime
			}
			else {
				data.time = this.roundTime(this.upcomingtime, 30)
			}

			this.signupService.updateSession(data).subscribe(
				response => {
					this.sessionJoined = true;
				},
				err => {
					this.ShowSubmitLoader = false;
				}
			);

		}
		if (this.fiveMinuteTimer) {
			this.sessionJoined = true;

			let audioPlayer = <HTMLVideoElement>document.getElementById("joinSessionSound");
			audioPlayer.play();
		}
		else {
			let audioPlayer = <HTMLVideoElement>document.getElementById("joinSessionSound");
			audioPlayer.play();
			this.sessionJoined = true;
			this.timerStarted = true;
			var disableClass = document.getElementsByClassName('add-main-disable-class')
			for (var i = 0; i < disableClass.length; i++) {
				disableClass[i].classList.add('main-disable')
			}
			document.getElementById('privateSessionId').style.pointerEvents = "none";
		}

		// alert(this.sessionJoined)
	}


	roundTime(time, minutesToRound) {

		let [hours, minutes, seconds] = time.split(':');

		// var mm;
		if (minutes >= 30 && minutes < 55) {
			minutes = 30
		}
		if (minutes < 30) (
			minutes = "0" + 0
		)
		return hours + ":" + minutes + ":" + seconds;
		// hours = parseInt(hours);
		// minutes = parseInt(minutes);

		// // Convert hours and minutes to time in minutes
		// time = (hours * 60) + minutes; 

		// let rounded = Math.round(time / minutesToRound) * minutesToRound;
		// let rHr = ''+Math.floor(rounded / 60)
		// let rMin = ''+ rounded % 60

		// return rHr.padStart(2, '0')+':'+rMin.padStart(2, '0')
	}

	joinPrivateSession() {
		if (this.guestOrUser == "guest") {
			document.getElementById('signhupforsession').click()
		} else {
			if (this.focusText != "") {
				this.stopTimer = false
				var that = this;
				setTimeout(function () {
					that.joinPrivateSessionTimer(25, 0, 'start');
					// that.joinPrivateSessionTimer(0, 15, 'start');

					document.getElementById('publicSessionId').style.pointerEvents = "none";
				}, 100);
				this.privateTimer = true
				var disableClass = document.getElementsByClassName('add-main-disable-class')
				for (var i = 0; i < disableClass.length; i++) {
					disableClass[i].classList.add('main-disable')
				}
			} else {
				Swal.fire({
					title: 'Error!',
					text: 'Post what are you focusing on',
					icon: 'error',
					confirmButtonText: 'Ok'
				})
			}

		}

	}

	joinPrivateSessionTimer(minutes, seconds, flag) {
		if (flag == 'start') {
			var counter = document.getElementById("timer2");
			if (counter != null) {
				counter.innerHTML =
					minutes.toString() + ":" + (seconds < 10 ? "0" : "") + String(seconds);
				if (minutes == 0 && seconds == 0) {
					this.stopTimer = false;
					this.privateTimer = false;
					this.focusText = "";
					document.getElementById("timer2").innerHTML = "25:00";

					this.focusSession = this.focusSession + 1
					localStorage.setItem("focusSession", String(this.focusSession))
					this.updateFocusSessionCounter();
					this.showFeedback = true;
					document.getElementById('hideorshowADiv').style.display = "none"

					var disableClass = document.getElementsByClassName('add-main-disable-class')
					for (var i = 0; i < disableClass.length; i++) {
						disableClass[i].classList.remove('main-disable')
					}
					document.getElementById('publicSessionId').style.pointerEvents = "auto";
				}
				this.privateTimeCount['minutes'] = minutes
				this.privateTimeCount['seconds'] = seconds

				seconds--;
				if (seconds >= 0) {
					// console.log(seconds , minutes)
					var that = this;
					this.timeoutHandle = setTimeout(function () {
						// this.privateTimeCount[''] = 
						that.joinPrivateSessionTimer(minutes, seconds, 'start');
					}, 1000);
				} else {
					if (minutes >= 1) {
						var that = this;
						setTimeout(function () {
							that.joinPrivateSessionTimer(minutes - 1, 59, 'start');
						}, 1000);
					}
				}
			}
		} else {
			var that = this;
			setTimeout(function () {
				var counter = document.getElementById("timer2");
				counter.innerHTML =
					minutes.toString() + ":" + (seconds < 10 ? "0" : "") + String(seconds);
			}, 1001);
		}
	}

	stopPrivateSession() {
		this.stopTimer = true;
		var disableClass = document.getElementsByClassName('add-main-disable-class')
		for (var i = 0; i < disableClass.length; i++) {
			disableClass[i].classList.remove('main-disable')
		}
		document.getElementById('publicSessionId').style.pointerEvents = "auto";
		// var that = this;
		// setTimeout(function () {
		//   that.stopTimer = true;
		// }, 1001);
		// console.log(this.stopTimer )
	}

	ResetTimer() {
		this.stopTimer = true;
		this.privateTimer = false
		this.privateTimeCount['minutes'] = 25;
		this.privateTimeCount['seconds'] = 0;
		this.focusText = "";
		var disableClass = document.getElementsByClassName('add-main-disable-class')
		for (var i = 0; i < disableClass.length; i++) {
			disableClass[i].classList.remove('main-disable')
		}
		document.getElementById('publicSessionId').style.pointerEvents = "auto";
	}

	resumePrivateSession() {
		this.stopTimer = false;
		var that = this;
		setTimeout(function () {
			that.joinPrivateSessionTimer(that.privateTimeCount['minutes'], that.privateTimeCount['seconds'], 'start');
			var disableClass = document.getElementsByClassName('add-main-disable-class')
			document.getElementById('publicSessionId').style.pointerEvents = "none";
			for (var i = 0; i < disableClass.length; i++) {
				disableClass[i].classList.add('main-disable')
			}
		}, 1);

	}




	// sigup signin
	public socialSignIn(socialProvider: string) {
		let socialPlatformProvider;
		if (socialProvider == 'facebook') {
			socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
		} else if (socialProvider == 'google') {
			socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
		}

		this.OAuth.signIn(socialPlatformProvider).then(socialusers => {
			// console.log(socialProvider, socialusers);  
			// console.log(socialusers);  
			this.Savesresponse(socialusers);

		});
	}
	logout() {
		var uid = localStorage.getItem('id')
		var type = "user"
		// console.log(uid)
		var userStatusDatabaseRef = this.db.database.ref('/status/' + uid);

		var isOfflineForDatabase = {
			state: 'offline',
			// last_changed: this.db.ServerValue.TIMESTAMP,
			id: uid,
			type: type
		};
		userStatusDatabaseRef.set(isOfflineForDatabase);

		localStorage.removeItem('name')
		localStorage.removeItem('id')
		localStorage.removeItem('image')
		localStorage.removeItem('token')
		this.imageUser = "assets/images/dummy-profile.png";
		this.loggedInId = "";
		this.loggedInUserName = "";
		this.ResetTimer()
		this.checkGuestOrUser()
		this.sessionJoined = false
	}
	CheckEmail() {
		let data = {
			email: this.email,
			password: this.password,
		}
		this.ShowSubmitLoader = true;
		this.signupService.checkemail(data).subscribe(
			response => {
				// console.log("response", response)
				if (response['error'] == 1) {
					this.ShowSubmitLoader = false;
					this.responseError['email_taken'] = true;
					this.responseError['email_taken_message'] = response['data']['message'];
				} else {
					this.ShowSubmitLoader = false;

					document.getElementById("openSecondModel").click();
					//document.getElementById("headlineButton").click();
					this.responseError['email_taken'] = false;
					this.responseError['email_taken_message'] = "";

				}
			},
			err => {
				this.ShowSubmitLoader = false;
				console.log("err", err)
			}
		);
	}
	submitsignupform() {
		// console.log(this.email)
		// console.log(this.name)
		// console.log(this.password)
		// console.log(this.helpingWith)
		this.ShowSubmitLoader = true;
		if (this.login_type == 2) {
			var type = 2;
		} else {
			var type = 1;
		}
		if (this.email == null) {
			this.email = localStorage.getItem("social_email");
		}
		let data = {
			email: this.email,
			password: this.password,
			name: this.name,
			helping_with: this.helpingWith,
			login_type: type,
			headline: this.headline,
			selefDescription: this.selefDescription,
			social_id: this.social_id,
			image: this.social_image
		}
		this.signupService.signup(data).subscribe(
			response => {
				this.getFocusCounterAndJoinedDetail()
				if (response['error'] == 1) {
					this.ShowSubmitLoader = false;

				} else {
					this.ShowSubmitLoader = false;
					// Swal.fire({
					//   title: 'Success',
					//   text: 'Signed up successfully ! Please log in.',
					//   icon: 'success',
					//   confirmButtonText: 'OK'
					// })

					this.ShowSubmitLoader = false;
					if (response['error'] == 1) {
						this.responseError['emailErrorLogin'] = true;
						this.responseError['email_taken_message'] = response['message'];
					} else {
						this.responseError['emailErrorLogin'] = "";
						this.responseError['email_taken_message'] = "";
						localStorage.setItem("token", response['data']['token'])
						localStorage.setItem("id", response['data']['id'])
						localStorage.setItem("name", response['data']['name'])
						localStorage.setItem("image", response['data']['image'])
						localStorage.setItem("focusSession", response['data']['focus_session'])


						var str = response['data']['image'];
						var n = str.includes("https://");
						if (n) {
							this.imageUser = response['data']['image']
						} else {
							this.imageUser = this.varImgUrl + '' + response['data']['image'];
						}
						// this.imageUser =this.varImgUrl+''+response['data']['image']
						this.loggedInUserName = response['data']['name']
						this.loggedInId = response['data']['id']
						this.checkGuestOrUser();
						this.getFocusCounterAndJoinedDetail()
						var uid = localStorage.getItem('id')
						var type = "user"
						var userStatusDatabaseRef = this.db.database.ref('/status/' + uid);

						var isOnlineForDatabase = {
							state: 'online',
							id: uid,
							type: type
						};
						userStatusDatabaseRef.set(isOnlineForDatabase);
						document.getElementById('closeLoginModel').click();
					}

					document.getElementById('finalCloseModal').click();
					// document.getElementById('signinButton').click();
				}
			},
			err => {
				this.ShowSubmitLoader = false;
				console.log("err", err)
			}
		);
	}

	signin() {
		let data = {
			email: this.emailLogin,
			password: this.passwordLogin,
		}
		this.ShowSubmitLoader = true;
		this.signupService.signin(data).subscribe(
			response => {
				this.ShowSubmitLoader = false;
				if (response['error'] == 1) {
					this.responseError['emailErrorLogin'] = true;
					this.responseError['email_taken_message'] = response['message'];
				} else {
					this.responseError['emailErrorLogin'] = "";
					this.responseError['email_taken_message'] = "";
					localStorage.setItem("token", response['data']['token'])
					localStorage.setItem("id", response['data']['id'])
					localStorage.setItem("name", response['data']['name'])
					localStorage.setItem("image", response['data']['image'])
					localStorage.setItem("self_description", response['data']['headline'])
					localStorage.setItem("focusSession", response['data']['focus_session'])
					localStorage.setItem("breatingSession", String(response['data']['breathing_session']))

					var str = response['data']['image'];
					var n = str.includes("https://");
					if (n) {
						this.imageUser = response['data']['image']
					} else {
						this.imageUser = this.varImgUrl + '' + response['data']['image'];
					}
					// this.imageUser =this.varImgUrl+''+response['data']['image']
					this.loggedInUserName = response['data']['name']
					this.loggedInId = response['data']['id']
					this.checkGuestOrUser();
					this.getFocusCounterAndJoinedDetail()
					var uid = localStorage.getItem('id')
					var type = "user"
					var userStatusDatabaseRef = this.db.database.ref('/status/' + uid);

					var isOnlineForDatabase = {
						state: 'online',
						id: uid,
						type: type
					};
					userStatusDatabaseRef.set(isOnlineForDatabase);
					document.getElementById('closeLoginModel').click();
				}

			},
			err => {
				this.ShowSubmitLoader = false;
				console.log("err", err)
			}
		);
	}

	Savesresponse(socialusers: Socialusers) {
		this.signupService.socialSignup(socialusers).subscribe(
			response => {
				if (response['data'].length == 0) {
					// alert("no signed in")
					this.email = socialusers.email;
					localStorage.setItem("social_email", this.email)
					this.social_id = socialusers.id;
					this.social_image = socialusers.image;
					this.ShowSubmitLoader = false;
					this.login_type = 2;

					document.getElementById("openSecondModel").click();
					this.responseError['email_taken'] = false;
					this.responseError['email_taken_message'] = "";
					var close = document.getElementsByClassName('close');
					for (var i = 0; i < close.length; i++) {
						let element: HTMLElement = close[i] as HTMLElement;
						element.click();
					}
				} else {
					this.responseError['emailErrorLogin'] = "";
					this.responseError['email_taken_message'] = "";
					localStorage.setItem("token", response['data']['token'])
					localStorage.setItem("id", response['data']['id'])
					localStorage.setItem("name", response['data']['name'])
					localStorage.setItem("image", response['data']['image'])
					localStorage.setItem("self_description", response['data']['headline'])
					localStorage.setItem("focusSession", response['data']['focus_session'])


					var str = response['data']['image'];
					var n = str.includes("https://");
					if (n) {
						this.imageUser = response['data']['image']
					} else {
						this.imageUser = this.varImgUrl + '' + response['data']['image'];
					}
					// this.imageUser =this.varImgUrl+''+response['data']['image']
					this.loggedInUserName = response['data']['name']
					this.loggedInId = response['data']['id']
					this.checkGuestOrUser();
					this.getFocusCounterAndJoinedDetail()
					var uid = localStorage.getItem('id')
					var type = "user"
					var userStatusDatabaseRef = this.db.database.ref('/status/' + uid);

					var isOnlineForDatabase = {
						state: 'online',
						id: uid,
						type: type
					};
					userStatusDatabaseRef.set(isOnlineForDatabase);
					document.getElementById('closeLoginModel').click();
					var close = document.getElementsByClassName('close');
					for (var i = 0; i < close.length; i++) {
						let element: HTMLElement = close[i] as HTMLElement;
						element.click();
					}
					document.getElementById('closeLoginModel').click();
				}

			},
			err => {
				console.log("err", err)
			}
		);
	}

	openLogIn() {
		document.getElementById('signupCloseModal').click();
		document.getElementById('signinButton').click();
	}


	onlineOffline() {
		var uid = localStorage.getItem('id');
		var type = "user";
		var userStatusDatabaseRef = this.db.database.ref('/status/' + uid);

		var isOfflineForDatabase = {
			state: 'offline',
			id: uid,
			type: type
		};

		var isOnlineForDatabase = {
			state: 'online',
			id: uid,
			type: type
		};
		this.db.database.ref('.info/connected').on('value', function (snapshot) {
			if (snapshot.val() == false) {
				return;
			};
			userStatusDatabaseRef.onDisconnect().set(isOfflineForDatabase).then(function () {
				userStatusDatabaseRef.set(isOnlineForDatabase);
			});
		});
	}

	getLiveuser() {
		this.db.database.ref('/status/');
		const that = this;
		var user = [];
		this.db.database.ref('/status/').on('value', function (snapshot) {
			snapshot.forEach(function (childSnapshot) {
				var childData = childSnapshot.val();
				if (childData['state'] == 'online') {
					if (childData['id'] != localStorage.getItem('id')) {
						if (childData['id'] != undefined) {
							user.push(childData['id'])
							that.liveUserArrayCurrentFocus[childData['id']] = childData['current_focus']
						}
					}
				} else {
					var index = user.indexOf(childData['id']);
					var index2 = that.liveUserArray.indexOf(childData['id']);
					if (index > -1) {
						user.splice(index, 1);
					}
					if (index2 > -1) {
						that.liveUserArray.splice(index, 1);
					}
				}
			});
			user = that.getUnique(user);
			var arrayOnline = that.arr_diff(that.liveUserArray, user)
			that.liveUserArray = user;
			that.getOnlineUserData({ id: that.liveUserArray })
		})
	}

	getOnlineUserData(arrayOnline) {
		this.signupService.getOnlineUserDate(arrayOnline).subscribe(
			response => {
				var pgi = response['data']['data'].next_page_url
				if (pgi != undefined) {
					this.pagintionLiveUser = true;
				}
				this.liveUserShowUI = response['data']['data'];
			},
			err => {

			}
		);
	}

	arr_diff(a1, a2) {
		var a = [], diff = [];
		for (var i = 0; i < a1.length; i++) {
			a[a1[i]] = true;
		}
		for (var i = 0; i < a2.length; i++) {
			if (a[a2[i]]) {
				delete a[a2[i]];
			} else {
				a[a2[i]] = true;
			}
		}
		for (var k in a) {
			diff.push(k);
		}
		return diff;
	}


	getUnique(array) {
		var uniqueArray = [];
		for (var i = 0; i < array.length; i++) {
			if (uniqueArray.indexOf(array[i]) === -1) {
				uniqueArray.push(array[i]);
			}
		}
		return uniqueArray;
	}


	sendMessage(event) {
		if (event != null) {
			event.preventDefault();
		}

		var uid = localStorage.getItem('id')
		var str = localStorage.getItem('image');
		var n = str.includes("https://");
		if (n) {
			this.imageUser = localStorage.getItem('image')
		} else {
			this.imageUser = this.varImgUrl + '' + localStorage.getItem('image');
		}
		this.chatReference.add({ 'message': this.chatbox, 'sender': uid, 'time': Date.now(), 'name': this.loggedInUserName, 'read': false, 'image': this.imageUser });
		this.chatbox = ""
	}

	getAllChat() {
		var afs2 = this.afs;
		var that = this;
		this.postsCol = this.afs.collection('posts', ref => ref.orderBy('time'));
		that.posts = that.postsCol.valueChanges();

		that.posts.subscribe(res => {
			that.message = [];
			res.forEach(element1 => {
				this.message.push(element1)
			});
		})
	}

	postFocusText() {
		if (this.guestOrUser == "guest") {
			document.getElementById('signhupforsession').click()
		} else {
			if (this.focusText != "") {
				this.chatbox = "My Focus :" + this.focusText;
				this.sendMessage(null);
				let audioPlayer = <HTMLVideoElement>document.getElementById("postSound");
				audioPlayer.play();
				var uid = localStorage.getItem('id')
				var type = "user"
				var that = this;
				var userStatusDatabaseRef = this.db.database.ref('/status/' + uid);

				var isOfflineForDatabase = {
					state: 'online',
					id: uid,
					type: type,
					current_focus: that.focusText
				};
				userStatusDatabaseRef.set(isOfflineForDatabase);
			}
		}
	}

	getData(post) {
		post.current_focus = this.liveUserArrayCurrentFocus[post.id]
		this.profileData = post;
	}

	submitRating(rating) {
		// alert(rating)
		setTimeout(function (that) {
			// this.privateTimeCount[''] = 
			that.showFeedback = false
			document.getElementById('hideorshowADiv').style.display = ""
		}, 500, this);

	}

	converDate(dat) {
		var mydate = new Date(dat);
		return mydate.toDateString();
	}

	// changeImagehttps(image){
	//   // console.log(image)
	//   if(image == undefined )
	//   {
	//     return "";
	//   }
	//   var n = image.includes("http://52.220.250.55/");
	//   if(n){
	//     return   image.replace("http://52.220.250.55/", "https://focustogether.com/")
	//   }
	//   else{
	//     return image
	//   }
	//   // return image

	// }
}
