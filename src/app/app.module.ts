import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

import { GoogleLoginProvider, FacebookLoginProvider, AuthService } from 'angular-6-social-login';
import { SocialLoginModule, AuthServiceConfig } from 'angular-6-social-login';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { MindfulSessionComponent } from './mindful-session/mindful-session.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestore } from 'angularfire2/firestore';

import { AngularFireDatabaseModule } from 'angularfire2/database';
import { UpdateProfileComponent } from './update-profile/update-profile.component';
import { LocationStrategy, HashLocationStrategy, PathLocationStrategy } from '@angular/common';
import { AdminComponent } from './admin/admin.component';
import { AdminhomeComponent } from './admin/adminhome/adminhome.component';
import { VideoViewComponent } from './admin/video-view/video-view.component';
import { MessageComponent } from './admin/message/message.component';
import { HeaderComponent } from './admin/header/header.component';
import { WebcamModule } from 'ngx-webcam';
import { CoffeehouseComponent } from './coffeehouse/coffeehouse.component';
import { NgxYoutubePlayerModule } from 'ngx-youtube-player';
import { GtagModule } from 'angular-gtag';
import { FirecallComponent } from './firecall/firecall.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button'
import { CallInfoDialogComponents } from './dialog/callinfo-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from "@angular/material/form-field"
import { MatInputModule } from "@angular/material/input"
// import { ClipboardModule } from '@angular/cdk/clipboard';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CallService } from './firecall/call.service';
import { ClipboardModule } from 'ngx-clipboard';



export function socialConfigs() {
	const config = new AuthServiceConfig(
		[
			{
				id: FacebookLoginProvider.PROVIDER_ID,
				provider: new FacebookLoginProvider('936656163828257')
			},
			{
				id: GoogleLoginProvider.PROVIDER_ID,
				provider: new GoogleLoginProvider('227030689661-5nj0jcjv7avener8srj75v1lndl9m8uv.apps.googleusercontent.com')
			}
		]
	);
	return config;
}

const firebaseConfig = {
	// apiKey: "AIzaSyCznZHM78J36V99FhQZx7pYId72DXRdOwk",
	// authDomain: "cheeryapp-59c15.firebaseapp.com",
	// databaseURL: "https://cheeryapp-59c15-default-rtdb.firebaseio.com",
	// projectId: "cheeryapp-59c15",
	// storageBucket: "cheeryapp-59c15.appspot.com",
	// messagingSenderId: "1078806963574",
	// appId: "1:1078806963574:web:748e260d0e772d9bb08657",
	// measurementId: "G-537HHRT6CY"


	apiKey: "AIzaSyC64wPPlOc5F7rKd3mz4SavcSWxr33PoyM",
	authDomain: "focustogether-3e372.firebaseapp.com",
	projectId: "focustogether-3e372",
	storageBucket: "focustogether-3e372.appspot.com",
	messagingSenderId: "1023894439808",
	appId: "1:1023894439808:web:986214fc7b2e08011e6365",
	databaseURL: "https://focustogether-3e372-default-rtdb.firebaseio.com/",



};
@NgModule({
	declarations: [
		AppComponent,
		HomeComponent,
		MindfulSessionComponent,
		UpdateProfileComponent,
		AdminComponent,
		AdminhomeComponent,
		VideoViewComponent,
		MessageComponent,
		HeaderComponent,
		CoffeehouseComponent,
		FirecallComponent,
		CallInfoDialogComponents
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		HttpClientModule,
		FormsModule,
		AngularFireDatabaseModule,
		AngularFireModule.initializeApp(firebaseConfig),
		WebcamModule,
		GtagModule.forRoot({ trackingId: 'AW-1049912176/UWAMCJLhhoECEPDG0fQD', trackPageviews: true }),
		NgxYoutubePlayerModule.forRoot(),
		BrowserAnimationsModule,
		MatButtonModule,
		MatDialogModule,
		MatFormFieldModule,
		MatInputModule,
		ClipboardModule,
		MatSnackBarModule


	],
	providers: [AuthService,
		{
			provide: AuthServiceConfig,
			useFactory: socialConfigs
		}, AngularFirestore, { provide: LocationStrategy, useClass: PathLocationStrategy },CallService],
	bootstrap: [AppComponent],
	entryComponents: [
		CallInfoDialogComponents
	 ]
})
export class AppModule { }
