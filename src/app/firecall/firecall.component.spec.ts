import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirecallComponent } from './firecall.component';

describe('FirecallComponent', () => {
  let component: FirecallComponent;
  let fixture: ComponentFixture<FirecallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirecallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirecallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
