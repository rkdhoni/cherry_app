import { Component, ElementRef, OnDestroy, OnInit, ViewChild, ChangeDetectionStrategy } from '@angular/core';
// import { MatDialog } from '@angular/material/dialog';
import { MatDialog } from '@angular/material';
import { Observable, of } from 'rxjs';
import { filter, switchMap } from 'rxjs/operators';
import { CallService } from './call.service';
import { CallInfoDialogComponents, DialogData } from '../dialog/callinfo-dialog.component';
import { ActivatedRoute } from '@angular/router';
import { ClipboardService } from 'ngx-clipboard'
import { environment } from 'src/environments/environment';



@Component({
	selector: 'app-firecall',
	templateUrl: './firecall.component.html',
	styleUrls: ['./firecall.component.css'],
	changeDetection: ChangeDetectionStrategy.OnPush

})
export class FirecallComponent implements OnInit {

	public isCallStarted$: Observable<boolean>;
	private peerId: string;
	baseUrlVideo = environment.baseUrlVideo;
	base_url_media = "https://focustogether.com/cherry-backend/public/uploads/";
	video_file = "forest.mp4";


	@ViewChild('localVideo', null) localVideo: ElementRef<HTMLVideoElement>;
	@ViewChild('remoteVideo', null) remoteVideo: ElementRef<HTMLVideoElement>;

	constructor(private _clipboardService: ClipboardService, private route: ActivatedRoute, public dialog: MatDialog, private callService: CallService) {
		this.isCallStarted$ = this.callService.isCallStarted$;
		this.peerId = this.callService.initPeer();
	}

	if_caller;
	param_peer_value;
	invite_link;

	ngOnInit(): void {

		this.route.queryParams.subscribe(params => {
			/* this.param1 = params['param1'];
			this.param2 = params['param2']; */
			this.param_peer_value = params['id'];
		});
	
		// this.param_peer_value = this.route.snapshot.params.id;

		this.if_caller = 0

		this.callService.localStream$
			.pipe(filter(res => !!res))
			.subscribe(stream => this.localVideo.nativeElement.srcObject = stream)
		this.callService.remoteStream$
			.pipe(filter(res => !!res))
			.subscribe(stream => this.remoteVideo.nativeElement.srcObject = stream)

		// console.log(this.peerId)
		if (this.param_peer_value == undefined) {
			this.if_caller = 1
			this.invite_link = "https://192.168.1.4:4201/fireside/" + this.peerId
			// of(this.callService.establishMediaCall(this.peerId))
		}
		if (this.param_peer_value != undefined) {
			// this.showModal(true);
			var joinCall = true;
			let dialogData: DialogData = joinCall ? ({ peerId: this.baseUrlVideo + "?id=" + this.param_peer_value, joinCall: true  }) : ({ peerId: this.baseUrlVideo + "?id=" + this.peerId, joinCall: false });

			// joinCall ? of(this.callService.establishMediaCall(this.param_peer_value)) : of(this.callService.enableCallAnswer()).subscribe(_ => { });
			var that = this;
			setTimeout(function(){ that.callService.establishMediaCall(that.param_peer_value).then() }, 1000);

			

			// )}
			// const dialogRef = this.dialog.open(CallInfoDialogComponents, {
			// 	width: '250px',
			// 	data: dialogData
			// });
			// // var peerData = this.param_peer_value
			// dialogRef.afterClosed()
			// 	.pipe(
			// 		switchMap(peerData =>
			// 			joinCall ? of(this.callService.establishMediaCall(this.param_peer_value)) : of(this.callService.enableCallAnswer())
			// 		),
			// 	)
			// 	.subscribe(_ => { });
		}else{
			var joinCall = false;
			var that = this;
			setTimeout(function(){ that.callService.enableCallAnswer().then() }, 1000);
		}

		// alert(this.param_peer_value);
		// this.route.queryParams.subscribe((params)=> {
		// 	//check lead Id here
		// 	if(params['id']){
		// 	  console.log(params['id']);
		// 	} else {
		// 	  console.log('id not found in params')
		// 	}
		//   });

	}

	ngOnDestroy(): void {
		this.callService.destroyPeer();
	}

	public showModal(joinCall: boolean): void {
		let dialogData: DialogData = joinCall ? ({ peerId: null, joinCall: true }) : ({ peerId: this.baseUrlVideo + "?id=" + this.peerId, joinCall: false });
		const dialogRef = this.dialog.open(CallInfoDialogComponents, {
			width: '250px',
			data: dialogData
		});

		/* dialogRef.afterClosed()
			.pipe(
				switchMap(peerId =>
					joinCall ? of(this.callService.establishMediaCall(peerId)) : of(this.callService.enableCallAnswer())
				),
			)
			.subscribe(_ => { }); */
	}

	public endCall() {
		this.callService.closeMediaCall();
	}

	copied() {

	}

	copy(text: string) {
		this._clipboardService.copy(text)
	}


}
