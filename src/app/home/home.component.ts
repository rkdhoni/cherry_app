import { Component, OnInit } from '@angular/core';

import { GoogleLoginProvider, FacebookLoginProvider, AuthService } from 'angular-6-social-login';
import { SocialLoginModule, AuthServiceConfig } from 'angular-6-social-login';
import { Socialusers } from '../Models/socialusers'
// import { SocialloginService } from '../Service/sociallogin.service';  
import { Router, ActivatedRoute, Params } from '@angular/router';
import { BackendApiService } from '../services/backend-api.service';
import Swal from 'sweetalert2'
@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
	windowWidth: string;
	showSplash = true;
	guestOrUser: any = "";
	totalTime = 0
	breatingSeassion = 0;
	focusSession = 0;
	email: any = "";
	password: any = "";
	responseError: any = [];
	response;
	socialusers = new Socialusers();
	musicArray: any = [];
	chimesArray: any = [];
	videoname: any = "forest";
	mindfulBreathing: any;
	breatheIn: boolean = false;
	breatheHold: boolean = false;
	breatheOut: boolean = false;
	activeSession: any = ""
	mindDeepFocus: any;
	videoLoader: boolean = false
	volumeShow: boolean = false;
	volumeSlider: any = 50
	totalBreathingSeconds = 0
	totalBreathingSecondsCounter = 0
	timetoShow = 0;
	dayCounterJson: any;
	dayCounter: any
	dailyMessage: string = "";
	dailySound: string = "";
	playDailySound: boolean;
	audioDaily: any;
	ambientCheck: any = [];
	videoArrayToShow: any = [];
	base_url_media: any = ""
	ShowSubmitLoader: boolean = false;
	name: any = ""
	helpingWith: any = ""
	passwordLogin: any = "";
	emailLogin: any = ""
	loggedInUserName = ""
	headline = ""
	selefDescription = ""
	// for timer
	FULL_DASH_ARRAY = 283;
	WARNING_THRESHOLD = 10;
	ALERT_THRESHOLD = 5;
	COLOR_CODES = {
		info: {
			color: "green"
		},
		warning: {
			color: "orange",
			threshold: this.WARNING_THRESHOLD
		},
		alert: {
			color: "red",
			threshold: this.ALERT_THRESHOLD
		}
	};

	TIME_LIMIT = 200;
	timePassed = 0;
	timeLeft = this.TIME_LIMIT;
	timerInterval = null;
	remainingPathColor = this.COLOR_CODES.info.color;
	//for timer end here
	imageUser: any = "assets/images/dummy-profile.png";
	varImgUrl: any = "https://focustogether.com/cherry-backend/public/uploads/";


	social_id = ""
	social_image = ""
	login_type;

	constructor(public OAuth: AuthService,
		private router: Router, private signupService: BackendApiService) { }

	ngOnInit() {
		this.datCounter()
		this.getSelectedMusic()
		this.getMedia();

		this.checkGuestOrUser()
		setTimeout(() => {
			this.windowWidth = "-" + window.innerWidth + "px";

			setTimeout(() => {
				this.showSplash = !this.showSplash;
			}, 500);
		}, 2000);
		// console.log("localStorage.getItem('name')",localStorage.getItem('name'))
		// console.log("localStorage.getItem('name')",localStorage.getItem('id'))
		document.getElementsByTagName("body")[0].classList.remove("second-page");
		if (localStorage.getItem('image') != null) {
			var str = localStorage.getItem('image');
			var n = str.includes("https://");
			if (n) {
				this.imageUser = localStorage.getItem('image');
			} else {
				this.imageUser = this.varImgUrl + '' + localStorage.getItem('image');
			}

		}
		console.log("this.imageUser", this.imageUser)
	}
	logout() {
		localStorage.removeItem('name')
		localStorage.removeItem('id')
		localStorage.removeItem('image')
		localStorage.removeItem('token')
		this.login_type = 1;
		this.imageUser = "assets/images/dummy-profile.png";
		this.checkGuestOrUser()
	}
	ngOnDestroy() {
		var closeClass = document.getElementsByClassName('close')
		for (var i = 0; i < closeClass.length; i++) {
			var element: HTMLElement = document.getElementsByClassName('close')[i] as HTMLElement;
			element.click();
			// closeClass[i].click()
		}
	}

	checkMusic(music) {
		// console.log(this.ambientCheck);
		if (this.ambientCheck.includes(music)) {
			let audioPlayer = <HTMLVideoElement>document.getElementById(music + "Sound");
			audioPlayer.play();
			audioPlayer.volume = parseFloat(this.volumeSlider) / 100;
			// this.pauseOrPlay()
			return true;
		}
	}

	getOtherSelectedAudio() {
		var otherSelectedMusic = localStorage.getItem("musicCheckArray");
		setTimeout(function () {
			var classs = document.getElementsByClassName(otherSelectedMusic + 'Check');
			var ss: HTMLInputElement = <HTMLInputElement>classs[0];
			if (ss != undefined) {
				ss.click()
			}

		}, 2150);

	}
	getSelectedMusic() {
		var ambientSelectedMusic = localStorage.getItem("ambientCheck");
		if (ambientSelectedMusic != null) {
			this.ambientCheck = ambientSelectedMusic.split(',')
		}



		// console.log(otherSelectedMusic)
	}


	checkGuestOrUser() {

		if (localStorage.getItem('id') == null) {
			this.guestOrUser = "guest";

		} else {
			this.guestOrUser = "user";
			this.loggedInUserName = localStorage.getItem('name')
		}
		// console.log(this.guestOrUser)
		if (localStorage.getItem('timeSpent') == null) {
			this.totalTime = 0
		} else {
			this.totalTime = parseInt(localStorage.getItem('timeSpent'));
		}
		if (localStorage.getItem('breatingSession') == null) {
			this.breatingSeassion = 0
		} else {
			this.breatingSeassion = parseInt(localStorage.getItem('breatingSession'));
		}
		if (localStorage.getItem('focusSession') == null) {
			this.focusSession = 0
		} else {
			this.focusSession = parseInt(localStorage.getItem('focusSession'));
		}
		// if (localStorage.getItem('dayCounterJson') == null) {
		//   this.dayCounter = 1
		//   let today = new Date().toISOString().slice(0, 10)
		//   var data = {
		//     dayCounter: this.dayCounter,
		//     date: today
		//   }
		//   localStorage.setItem('dayCounterJson', JSON.stringify(data));

		// } else {
		//   var json = JSON.parse(localStorage.getItem('dayCounterJson'));
		//   this.dayCounter = parseInt(json.dayCounter)
		//   let today = new Date().toISOString().slice(0, 10)
		//   if (json.date != today) {
		//     this.dayCounter = this.dayCounter + 1
		//     var data = {
		//       dayCounter: this.dayCounter,
		//       date: today
		//     }
		//     localStorage.setItem('dayCounterJson', JSON.stringify(data));

		//   }



		//   // console.log(localStorage.getItem('dayCounterJson'));
		// }


		var that = this;

		var that = this;
		setInterval(() => {                           //<<<---using ()=> syntax
			that.totalTime = that.totalTime + 60;
			localStorage.setItem("timeSpent", String(that.totalTime))
		}, 60000);
	}

	datCounter() {
		this.dayCounter = 1;
		this.GetDailyMessage();
		// alert("Sdf")
		let today = new Date().toISOString().slice(0, 10)
		var todayDate = {
			today: today
		}
		this.signupService.dateCounter(todayDate).subscribe(
			response => {
				this.dayCounter = response['message']['day_counter'];
				if (response['message']['last_date'] != today) {
					var data = {
						dayCounter: this.dayCounter,
						today: today
					}
					this.signupService.updatedateCounter(data).subscribe(
						response => {
							this.dayCounter = this.dayCounter + 1
							this.GetDailyMessage();
						},
						err => {
						}
					);
				} else {
					this.GetDailyMessage();
				}
			},
			err => {
			}
		);
	}

	public socialSignIn(socialProvider: string) {
		let socialPlatformProvider;
		if (socialProvider == 'facebook') {
			socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
		} else if (socialProvider == 'google') {
			socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
		}

		this.OAuth.signIn(socialPlatformProvider).then(socialusers => {
			console.log(socialProvider, socialusers);
			// console.log(socialusers);  
			this.Savesresponse(socialusers);

		});
	}

	submitsignupform() {

		this.ShowSubmitLoader = true;
		if (this.login_type == 2) {
			var type = 2;
		} else {
			var type = 1;
		}
		if(this.email == null){
			this.email = localStorage.getItem("social_email");
		}
		let data = {
			email: this.email,
			password: this.password,
			name: this.name,
			helping_with: this.helpingWith,
			login_type: type,
			headline: this.headline,
			selefDescription: this.selefDescription,
			social_id: this.social_id,
			image: this.social_image
		}
		this.signupService.signup(data).subscribe(
			response => {
				if (response['error'] == 1) {
					this.ShowSubmitLoader = false;

				} else {
					this.ShowSubmitLoader = false;
					// Swal.fire({
					//   title: 'Success',
					//   text: 'Signed up successfully ! Please log in.',
					//   icon: 'success',
					//   confirmButtonText: 'OK'
					// })
					this.responseError['emailErrorLogin'] = "";
					this.responseError['email_taken_message'] = "";
					localStorage.setItem("token", response['data']['token'])
					localStorage.setItem("id", response['data']['id'])
					localStorage.setItem("name", response['data']['name'])
					localStorage.setItem("image", response['data']['image'])



					var str = response['data']['image'];
					var n = str.includes("https://");
					if (n) {
						this.imageUser = response['data']['image']
					} else {
						this.imageUser = this.varImgUrl + '' + response['data']['image'];
					}

					// this.imageUser =this.varImgUrl+''+response['data']['image']
					this.checkGuestOrUser()
					document.getElementById('closeLoginModel').click();
					document.getElementById('finalCloseModal').click();
					// document.getElementById('signinButton').click();
				}
			},
			err => {
				this.ShowSubmitLoader = false;
				console.log("err", err)
			}
		);
	}

	signin() {
		let data = {
			email: this.emailLogin,
			password: this.passwordLogin,
		}
		this.ShowSubmitLoader = true;
		this.signupService.signin(data).subscribe(
			response => {
				this.ShowSubmitLoader = false;
				if (response['error'] == 1) {
					this.responseError['emailErrorLogin'] = true;
					this.responseError['email_taken_message'] = response['message'];
				} else {
					this.responseError['emailErrorLogin'] = "";
					this.responseError['email_taken_message'] = "";
					localStorage.setItem("token", response['data']['token'])
					localStorage.setItem("id", response['data']['id'])
					localStorage.setItem("name", response['data']['name'])
					localStorage.setItem("image", response['data']['image'])
					localStorage.setItem("self_description", response['data']['headline'])
					localStorage.setItem("breatingSession", String(response['data']['breathing_session']))

					this.datCounter()

					var str = response['data']['image'];
					var n = str.includes("https://");
					if (n) {
						this.imageUser = response['data']['image']
					} else {
						this.imageUser = this.varImgUrl + '' + response['data']['image'];
					}

					// this.imageUser =this.varImgUrl+''+response['data']['image']
					this.checkGuestOrUser()
					document.getElementById('closeLoginModel').click();
				}

			},
			err => {
				this.ShowSubmitLoader = false;
				console.log("err", err)
			}
		);
	}


	getMedia() {
		this.signupService.getMedia().subscribe(
			response => {
				this.videoArrayToShow = response['video'];
				this.videoname = this.videoArrayToShow[0].video_name
				this.base_url_media = response['base_url'];
				// console.log("response",response);
			},
			err => {
				console.log("err", err)
			}
		);
	}


	GetDailyMessage() {
		var group = 1;
		if (this.focusSession > this.breatingSeassion) {
			group = 2
		}
		var data = {
			dayCounter: this.dayCounter,
			user_group: group
		}
		this.signupService.GetDailyMessage(data).subscribe(
			response => {
				this.dailyMessage = response['data'][0]['message'];

				setTimeout(() => {
					this.dailySound = response['data'][0]['audio'];

					this.audioDaily = new Audio(this.dailySound);
					// this.audioDaily.play();
					// alert(this.audioDaily.duration)
					// this.audioDaily.pause();
					this.audioDaily.volume = parseFloat(this.volumeSlider) / 100;
					this.playDailySound = false;

				}, 2100);
				this.getOtherSelectedAudio()

			},
			err => {
				console.log("err", err)
			}
		);
	}

	pauseOrPlay() {
		if (this.playDailySound) {
			this.audioDaily.pause();
			// this.audioDaily.currentTime = 0;
		} else {
			this.audioDaily.play();
			this.audioDaily.volume = parseFloat(this.volumeSlider) / 100;
		}
		this.playDailySound = !this.playDailySound;
		var that = this;
		//this.audioDaily.addEventListener('ended',this.myHandler(),false);
		this.audioDaily.onended = function (e) {
			// alert("Sd")
			that.playDailySound = !that.playDailySound;
		};
		// this.audioDaily.onloadedmetadata = function() {
		//alert(new Date(this.audioDaily.duration * 1000).toISOString().substr(11, 8));
		// };
	}



	Savesresponse(socialusers: Socialusers) {
		this.signupService.socialSignup(socialusers).subscribe(
			response => {
				if (response['data'].length == 0) {
					// alert("no signed in")
					this.email = socialusers.email;
					localStorage.setItem("social_email", this.email)
					this.social_id = socialusers.id;
					this.social_image = socialusers.image;
					this.ShowSubmitLoader = false;
					this.login_type = 2;

					document.getElementById("openSecondModel").click();
					this.responseError['email_taken'] = false;
					this.responseError['email_taken_message'] = "";
					var close = document.getElementsByClassName('close');
					for (var i = 0; i < close.length; i++) {
						let element: HTMLElement = close[i] as HTMLElement;
						element.click();
					}
				} else {
					this.responseError['emailErrorLogin'] = "";
					this.responseError['email_taken_message'] = "";
					localStorage.setItem("token", response['data']['token'])
					localStorage.setItem("id", response['data']['id'])
					localStorage.setItem("name", response['data']['name'])
					localStorage.setItem("image", response['data']['image'])
					localStorage.setItem("self_description", response['data']['headline'])

					var str = response['data']['image'];
					var n = str.includes("https://");
					if (n) {
						this.imageUser = response['data']['image']
					} else {
						this.imageUser = this.varImgUrl + '' + response['data']['image'];
					}
					// this.imageUser =this.varImgUrl+''+response['data']['image']

					this.checkGuestOrUser()
					var close = document.getElementsByClassName('close');
					for (var i = 0; i < close.length; i++) {
						let element: HTMLElement = close[i] as HTMLElement;
						element.click();
					}
					document.getElementById('closeLoginModel').click();
				}

			},
			err => {
				console.log("err", err)
			}
		);
	}


	CheckEmail() {
		let data = {
			email: this.email,
			password: this.password,
		}
		this.ShowSubmitLoader = true;
		this.signupService.checkemail(data).subscribe(
			response => {
				console.log("response", response)
				if (response['error'] == 1) {
					this.ShowSubmitLoader = false;
					this.responseError['email_taken'] = true;
					this.responseError['email_taken_message'] = response['data']['message'];
				} else {
					this.ShowSubmitLoader = false;
					document.getElementById("openSecondModel").click();
					this.responseError['email_taken'] = false;
					this.responseError['email_taken_message'] = "";

				}
			},
			err => {
				this.ShowSubmitLoader = false;
				console.log("err", err)
			}
		);
	}


	ambientMusic(type, event) {
		// console.log(this.ambientCheck)
		// console.log(event.target.checked)
		if (event.target.checked) {
			this.ambientCheck.push(type)
		} else {
			var index = this.ambientCheck.indexOf(type);
			if (index !== -1) {
				this.ambientCheck.splice(index, 1);
			}
		}
		let audioPlayer = <HTMLVideoElement>document.getElementById(type + "Sound");
		if (event.target.checked) {
			audioPlayer.play();
			audioPlayer.volume = parseFloat(this.volumeSlider) / 100;
		} else {
			audioPlayer.pause();
			audioPlayer.currentTime = 0;
		}
		// / console.log(this.ambientCheck)
		localStorage.setItem("ambientCheck", this.ambientCheck);

	}


	music(type, event) {

		this.musicArray = [];
		var uncheck = document.getElementsByClassName('musicCheckbox');
		for (var i = 0; i < uncheck.length; i++) {
			var ss: HTMLInputElement = <HTMLInputElement>uncheck[i];
			// console.log(ss.className);
			// console.log(event.target.className);
			if (ss.className != event.target.className) {
				ss.checked = false;
			}

		}
		// event.target.checked = true;
		if (event.target.checked) {
			localStorage.setItem("musicCheckArray", type);
			var o = {};
			o[type] = true;
			this.musicArray.push(o)
			// console.log(this.musicArray,type)
			setTimeout(() => {
				let audioPlayer = <HTMLVideoElement>document.getElementById(type + "Sound");
				audioPlayer.play();
				audioPlayer.volume = this.volumeSlider / 100;
			}, 100);

		} else {
			localStorage.removeItem("musicCheckArray");
			let audioPlayer = <HTMLVideoElement>document.getElementById(type + "Sound");
			audioPlayer.pause();
			audioPlayer.currentTime = 0;
			this.musicArray = [];
		}

	}


	videoChange(type, event) {
		this.videoLoader = true;
		this.videoname = type;
		var video = <HTMLVideoElement>document.getElementById("videoLoaded");

		video.addEventListener('loadeddata', (e) => {
			//Video should now be loaded but we can add a second check

			if (video.readyState >= 3) {
				this.videoLoader = false;
			}

		});


		// alert("rr")
	}

	loadTimer() {
		document.getElementById("appShower").innerHTML = `
      <div class="base-timer">
        <svg class="base-timer__svg" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
          <g class="base-timer__circle">
            <circle class="base-timer__path-elapsed" cx="50" cy="50" r="45"></circle>
            <path
              id="base-timer-path-remaining"
              stroke-dasharray="283"
              class="base-timer__path-remaining ${this.remainingPathColor}"
              d="
                M 50, 50
                m -45, 0
                a 45,45 0 1,0 90,0
                a 45,45 0 1,0 -90,0
              "
            ></path>
          </g>
        </svg>
        <span id="base-timer-label" class="base-timer__label">${this.formatTime(
			this.timeLeft
		)}</span>
      </div>
      `;

		this.startTimer();

	}


	startTimer() {
		this.timerInterval = setInterval(() => {
			this.timePassed = this.timePassed += 1;
			this.timeLeft = this.TIME_LIMIT - this.timePassed;
			document.getElementById("base-timer-label").innerHTML = this.formatTime(
				this.timeLeft
			);
			this.setCircleDasharray();
			this.setRemainingPathColor(this.timeLeft);

			if (this.timeLeft === 0) {
				this.onTimesUp();
				this.timePassed = 0;
				if (this.activeSession == "mindfulbreathing") {
					this.breatingSeassion = this.breatingSeassion + 1
					localStorage.setItem("breatingSession", String(this.breatingSeassion))
				}
				else if (this.activeSession == "minddeepFocus") {
					this.focusSession = this.focusSession + 1
					localStorage.setItem("focusSession", String(this.focusSession))
				}
			}
			if (this.timeLeft === 1) {
				let audioPlayer = <HTMLVideoElement>document.getElementById("bellSound");

				audioPlayer.play()
			}
		}, 1000);
	}
	formatTime(time) {
		let minutes = String(Math.floor(time / 60));
		let seconds = String(time % 60);

		if (parseFloat(seconds) < 10) {
			seconds = `0${seconds}`;
		}
		if (parseFloat(minutes) < 10) {
			minutes = `0${minutes}`;
		}

		return `${minutes}:${seconds}`;
	}
	onTimesUp() {
		clearInterval(this.timerInterval);

		if (this.activeSession == "mindfulbreathing") {
			this.breatheHold = false;
			this.breatheIn = false;
			this.breatheOut = false;
			document.getElementById('appShower').innerHTML = "";
			document.getElementById("openCalmPopup").click()
		} else {
			document.getElementById('appShower').innerHTML = "";
			document.getElementById("openDeepfocusPopup").click()

		}
	}

	setRemainingPathColor(timeLeft) {
		const { alert, warning, info } = this.COLOR_CODES;
		if (timeLeft <= alert.threshold) {
			document
				.getElementById("base-timer-path-remaining")
				.classList.remove(warning.color);
			document
				.getElementById("base-timer-path-remaining")
				.classList.add(alert.color);
		} else if (timeLeft <= warning.threshold) {
			document
				.getElementById("base-timer-path-remaining")
				.classList.remove(info.color);
			document
				.getElementById("base-timer-path-remaining")
				.classList.add(warning.color);
		}
	}

	calculateTimeFraction() {
		const rawTimeFraction = this.timeLeft / this.TIME_LIMIT;
		return rawTimeFraction - (1 / this.TIME_LIMIT) * (1 - rawTimeFraction);
	}

	setCircleDasharray() {
		const circleDasharray = `${(
			this.calculateTimeFraction() * this.FULL_DASH_ARRAY
		).toFixed(0)} 283`;
		document
			.getElementById("base-timer-path-remaining")
			.setAttribute("stroke-dasharray", circleDasharray);
	}

	openMindfulClock() {
		// this.TIME_LIMIT = 0
		// this.timeLeft = 0
		// this.timePassed = 0;
		// clearInterval(this.timerInterval);
		// this.TIME_LIMIT= this.mindfulBreathing * 60;
		// this.timeLeft=this.TIME_LIMIT
		// this.loadTimer()
		this.totalBreathingSeconds = this.mindfulBreathing * 60;
		var seconds = this.mindfulBreathing * 60;
		this.breatheIn = true;
		this.breatheHold = false;
		this.breatheOut = false;
		this.breatheInmethod();
		this.activeSession = "mindfulbreathing";
		this.mindDeepFocus = undefined;
		this.mindfulBreathing = undefined;
		document.getElementById('appShower').innerHTML = "";
		var that = this;
		this.timetoShow = 2;
		this.totalBreathingSecondsCounter = this.totalBreathingSeconds
		var timbreathingInterval = setInterval(() => {
			if (that.totalBreathingSeconds > 0) {
				if (that.timetoShow == 0) {
					that.timetoShow = 2;
				} else {
					that.timetoShow = that.timetoShow - 1;
				}
			}
			else {
				clearInterval(timbreathingInterval)
				this.breatheIn = false;
				this.breatheHold = false;
				this.breatheOut = false;
				this.UpdateBreathingSession()
				this.breatingSeassion = this.breatingSeassion + 1
				localStorage.setItem("breatingSession", String(this.breatingSeassion))

			}
			this.totalBreathingSecondsCounter = this.totalBreathingSecondsCounter - 1
			console.log("this.totalBreathingSecondsCounter--", seconds, this.totalBreathingSecondsCounter)
			// console.log("this.totalBreathingSecondsCounter",this.totalBreathingSecondsCounter)
			if (this.totalBreathingSecondsCounter == seconds - (seconds - 1)) {
				let audioPlayer = <HTMLVideoElement>document.getElementById("bellSound");

				audioPlayer.play()
			}
		}, 1000);
		setTimeout(function () {
			that.breatheHold = false;
			that.breatheIn = false;
			that.breatheOut = false;
			that.totalBreathingSeconds = 0;

		}, that.totalBreathingSeconds * 1000);
	}

	UpdateBreathingSession() {
		if (this.guestOrUser != "guest") {
			this.signupService.updateBreathingSessionCounter().subscribe(
				response => {
					console.log("done")
				},
				err => {
					this.ShowSubmitLoader = false;
				}
			);
		}
	}
	breatheInmethod() {
		var that = this;
		setTimeout(function () {
			if (that.totalBreathingSeconds > 0) {
				that.breatheHold = true;
				that.breatheIn = false;
				that.breatheOut = false;
				that.breatheHoldMethod();
			} else {
				that.breatheHold = false;
				that.breatheIn = false;
				that.breatheOut = false;
				// document.getElementById('appShower').innerHTML = "";
			}
			that.totalBreathingSeconds = that.totalBreathingSeconds - 3;
		}, 3000);
	}

	breatheHoldMethod() {
		var that = this;
		setTimeout(function () {
			if (that.totalBreathingSeconds > 0) {
				that.breatheHold = false;
				that.breatheIn = false;
				that.breatheOut = true;
				that.breatheOutMethod();
			} else {
				that.breatheHold = false;
				that.breatheIn = false;
				that.breatheOut = false;

				//document.getElementById('appShower').innerHTML = "";
			}
			that.totalBreathingSeconds = that.totalBreathingSeconds - 3;
		}, 3000);
	}


	breatheOutMethod() {
		var that = this;
		setTimeout(function () {
			if (that.totalBreathingSeconds > 0) {
				that.breatheHold = false;
				that.breatheIn = true;
				that.breatheOut = false;
				that.breatheInmethod();
			} else {
				that.breatheHold = false;
				that.breatheIn = false;
				that.breatheOut = false;
				//document.getElementById('appShower').innerHTML = "";
			}
			that.totalBreathingSeconds = that.totalBreathingSeconds - 3;
		}, 3000);
	}

	openminddeepfocusClock() {
		this.TIME_LIMIT = 0
		this.timeLeft = 0
		this.timePassed = 0;
		clearInterval(this.timerInterval);
		this.breatheHold = false;
		this.breatheIn = false;
		this.breatheOut = false;
		this.totalBreathingSeconds = 0
		document.getElementById('closeDeepFocusPopup').click();
		this.TIME_LIMIT = this.mindDeepFocus * 60;
		this.totalBreathingSeconds = 0
		// this.TIME_LIMIT= 0.1 * 60;
		this.timeLeft = this.TIME_LIMIT
		this.loadTimer();
		this.activeSession = "minddeepFocus";
		this.mindfulBreathing = undefined;

	}

	VolumeBarShowHide() {
		this.volumeShow = !this.volumeShow;
		console.log("sdf")
	}

	ChangeVolume() {
		var uncheck = document.getElementsByTagName('audio');
		for (var i = 0; i < uncheck.length; i++) {
			var ss: HTMLAudioElement = <HTMLAudioElement>uncheck[i];
			ss.volume = this.volumeSlider / 100;
		}
		this.audioDaily.volume = this.volumeSlider / 100;
		this.VolumeBarShowHide()

	}

}
