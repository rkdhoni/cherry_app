import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { MindfulSessionComponent } from './mindful-session/mindful-session.component';
import { UpdateProfileComponent } from './update-profile/update-profile.component';
import { AdminComponent } from './admin/admin.component';
import { AdminhomeComponent } from './admin/adminhome/adminhome.component';
import { VideoViewComponent } from './admin/video-view/video-view.component';
import { MessageComponent } from './admin/message/message.component';
import { CoffeehouseComponent } from './coffeehouse/coffeehouse.component';
import { FirecallComponent } from './firecall/firecall.component';


const routes: Routes = [
	//{ path: '', component: CoffeehouseComponent },
	{ path: '', component: FirecallComponent },
	/* {
		path: ':id',
		component: FirecallComponent
	}, */
	{ path: 'session', component: MindfulSessionComponent },
	{ path: 'update-profile', component: UpdateProfileComponent },
	{ path: 'admin', component: AdminComponent },
	{ path: 'admin/home', component: AdminhomeComponent },
	{ path: 'admin/videos', component: VideoViewComponent },
	{ path: 'admin/message', component: MessageComponent },
	{ path: 'nature', component: HomeComponent },
	{ path: 'fireside', component: FirecallComponent },
	/* {
		path: 'fireside/:id',
		component: FirecallComponent
	} */
];

@NgModule({
	imports: [RouterModule.forRoot(routes, { useHash: false })],
	exports: [RouterModule]
})
export class AppRoutingModule { }
