import { Inject } from '@angular/core';
import { Component } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ClipboardService } from 'ngx-clipboard'
import { environment } from '../../environments/environment';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-call-info-dialog',
    templateUrl: './callinfo-dialog.component.html',
    styleUrls: ['./callinfo-dialog.component.scss']
})
export class CallInfoDialogComponents {
    if_copied = 0
    baseUrlVideo=environment.baseUrlVideo;
    param_peer_value;
    constructor(
        private _clipboardService: ClipboardService,public dialogRef: MatDialogRef<CallInfoDialogComponents>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData, private route: ActivatedRoute,
        private _snackBar: MatSnackBar
    ) { 

        // this.param_peer_value = this.route.snapshot.params.id;
        this.route.queryParams.subscribe(params => {
			/* this.param1 = params['param1'];
			this.param2 = params['param2']; */
			this.param_peer_value = params['id'];
        });
        
        // console.log("this.data",this.data)
        if(this.data.joinCall && this.data.peerId != undefined){
            console.log(this.if_copied)
            console.log(this.data.joinCall)
            // document.getElementById('submitFormId').click();
        }
        
    }

    public showCopiedSnackBar(peerId) {      
        this.if_copied = 1
        this._clipboardService.copy(peerId)  
        this._snackBar.open('Peer ID Copied!', 'Hurrah', {
        duration: 1000,
        horizontalPosition: 'center',
        verticalPosition: 'top'
      });
    }
}

export interface DialogData {
    peerId?: string;
    joinCall: boolean
    // baseUrlVideo:"https://focustogether.com/cherrystage/fireside";
}