import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class BackendApiService {
  rootUrl: string;
  isLoading: boolean;
  constructor(private http: HttpClient, private router: Router, ) { 
    
  }

  signup(data){
      this.rootUrl = environment.baseUrl;
      // let headers = new HttpHeaders({
      // 'Content-Type': 'application/json',
      // 'Authorization': 'Bearer '+api.api_token });
      // let options = { headers: headers };
      return this.http.post(this.rootUrl+'/signup',data );
  }
  signin(data){
    this.rootUrl = environment.baseUrl;
    // let headers = new HttpHeaders({
    // 'Content-Type': 'application/json',
    // 'Authorization': 'Bearer '+api.api_token });
    // let options = { headers: headers };
    return this.http.post(this.rootUrl+'/signin',data );
  }

  checkemail(data){
    this.rootUrl = environment.baseUrl;
      // let headers = new HttpHeaders({
      // 'Content-Type': 'application/json',
      // 'Authorization': 'Bearer '+api.api_token });
      // let options = { headers: headers };
      return this.http.post(this.rootUrl+'/checkMail',data );
  }
  GetDailyMessage(data){
    this.rootUrl = environment.baseUrl;
    return this.http.post(this.rootUrl+'/GetDailyMessage',data );
  }

  getMedia(){
    this.rootUrl = environment.baseUrl;
    return this.http.post(this.rootUrl+'/media',null );
  }
  getUserData(){
    this.rootUrl = environment.baseUrl;
    let headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'Bearer '+localStorage.getItem('token') });
    let options = { headers: headers };
    return this.http.post(this.rootUrl+'/getUserData',null, options);
  }
  updateUserData(data){
    this.rootUrl = environment.baseUrl;

    // let headers = new HttpHeaders({
    //   "Accept": "multipart/form-data" ,
    //   'Authorization': 'Bearer '+api_token });
    //   let options = { headers: headers };

    let headers = new HttpHeaders({
    "Accept": "multipart/form-data" ,
    'Authorization': 'Bearer '+localStorage.getItem('token') });
    let options = { headers: headers };
    return this.http.post(this.rootUrl+'/updateUserData',data, options);
  }
  getOnlineUserDate(data){
    this.rootUrl = environment.baseUrl;
    let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+localStorage.getItem('token') });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/getOnlineUserDate',data, options);
  }

  updateSession(data){
    this.rootUrl = environment.baseUrl;
    let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+localStorage.getItem('token') });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/bookSession',data, options);
  }
  leaveSession(){
    this.rootUrl = environment.baseUrl;
    let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+localStorage.getItem('token') });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/leaveSession',null, options);
  }
  getSessionData(data){
    this.rootUrl = environment.baseUrl;
    let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+localStorage.getItem('token') });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/getSessionData',data, options);
  }
  updateFocusSessionCounter(){
    this.rootUrl = environment.baseUrl;
    let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+localStorage.getItem('token') });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/updateFocusSessionCounter',null, options);
  }

  socialSignup(data){ 
    this.rootUrl = environment.baseUrl;
    let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+localStorage.getItem('token') });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/socialSignup',data, options);
  }

  dateCounter(data){
    this.rootUrl = environment.baseUrl;
    let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+localStorage.getItem('token') });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/dateCounter',data, options);
  }
  updatedateCounter(data){
    this.rootUrl = environment.baseUrl;
    let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+localStorage.getItem('token') });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/updatedateCounter',data, options);
  }



  // api for admin
  getAllUsers(data){
    this.rootUrl = environment.baseUrl;
    let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+localStorage.getItem('role') });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/getAllUsers',data, options);
  }

  siginAdmin(data){
    this.rootUrl = environment.baseUrl;
    let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+localStorage.getItem('role') });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/siginAdmin',data, options);
  }
  getvideos(){
    this.rootUrl = environment.baseUrl;
    let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+localStorage.getItem('role') });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/media',null, options);
  }
  deleteVideos(data){
    this.rootUrl = environment.baseUrl;
    let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+localStorage.getItem('role') });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/deleteVideos',data, options);
  }
  uploadVideo(data){
    this.rootUrl = environment.baseUrl;
    let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+localStorage.getItem('role') });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/uploadVideo',data, options);
  }
  uploadVideoedit(data){
    this.rootUrl = environment.baseUrl;
    let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+localStorage.getItem('role') });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/uploadVideoedit',data, options);
  }
  getMessages(){
    this.rootUrl = environment.baseUrl;
    let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+localStorage.getItem('role') });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/getMessages',null, options);
  }
  addMessage(data){
    this.rootUrl = environment.baseUrl;
    let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+localStorage.getItem('role') });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/addMessage',data, options);
  }
  updateBreathingSessionCounter(){
    this.rootUrl = environment.baseUrl;
    let headers = new HttpHeaders({
      "Accept": "multipart/form-data" ,
      'Authorization': 'Bearer '+localStorage.getItem('token') });
      let options = { headers: headers };
      return this.http.post(this.rootUrl+'/updateBreathingSessionCounter',null, options);
  }
   ////console.log.log(api);
   
}
